﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AngelMono.Physics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AngelMono.Infrastructure
{
    public class GameManager : Renderable
    {
        public override void Render(GameTime aTime, Camera aCamera, GraphicsDevice aDevice, SpriteBatch aBatch) { }
        public override void Update(GameTime aTime) { }
        public virtual bool IsProtectedFromUnloadAll(Renderable renderable) { return false; }
    }
}
