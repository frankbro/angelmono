﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace AngelMono.Infrastructure
{
    public interface IAngelCustomSerializable
    {
        void CustomSerialize(TextWriter stream);

    }
}
