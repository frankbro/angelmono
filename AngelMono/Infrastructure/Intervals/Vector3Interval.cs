﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace AngelMono.Infrastructure.Intervals
{
    public class Vector3Interval : Interval<Vector3>
    {
        public Vector3Interval(Vector3 start, Vector3 end, float duration, bool smooth)
            : base(start, end, duration, smooth) { }

        public override Vector3 Step(float dt)
        {
            if (!_shouldStep)
            {
                return _current;
            }

            _timer += dt;

            if (_timer >= _duration)
            {
                _current = _end;
                _shouldStep = false;
            }
            else
            {
                float stepRatio = _timer / _duration;
                if (_smoothStep)
                {
                    // Smooth step
                    _current = new Vector3(
                        MathHelper.SmoothStep(_start.X, _end.X, stepRatio), 
                        MathHelper.SmoothStep(_start.Y, _end.Y, stepRatio),
                        MathHelper.SmoothStep(_start.Z, _end.Z, stepRatio)
                    );
                }
                else
                {
                    // Simple LERP
                    _current = new Vector3(
                        MathHelper.Lerp(_start.X, _end.X, stepRatio), 
                        MathHelper.Lerp(_start.Y, _end.Y, stepRatio),
                        MathHelper.Lerp(_start.Z, _end.Z, stepRatio)
                    );
                }
            }
            return _current;
        }
    }
}
