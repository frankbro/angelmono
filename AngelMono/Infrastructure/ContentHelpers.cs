﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using System.IO;
using AngelMono.Infrastructure.Logging;

namespace AngelMono.Infrastructure
{
    public class ContentHelpers
    {
        public static bool TextureExists(string asFileName)
        {
#if WINDOWS
            // These are the extensions we'll try before attempting Content.Load
            string[] extensions = new string[] { ".png", ".jpg", ".bmp" };
            foreach (string ext in extensions)
            {
                string filename = "Content\\" + asFileName + ext;
                if (File.Exists(filename))
                    return true;
            }
#endif

            if (File.Exists(Path.Combine("Content", asFileName + ".xnb")))
                return true;


            return false;
        }

        public static Texture2D LoadTexture(string asFileName)
        {
            Texture2D texture = null;
#if WINDOWS
            // These are the extensions we'll try before attempting Content.Load
            string[] extensions = new string[] { ".png", ".jpg", ".bmp" };
            foreach (string ext in extensions)
            {
                string filename = "Content\\" + asFileName + ext;
                if (File.Exists(filename))
                {
                    using (FileStream stream = new FileStream(filename, FileMode.Open))
                    {
                        texture = Texture2D.FromStream(World.Instance.Game.GraphicsDevice, stream);
                    }
                }
            }
#endif

            if (texture == null)
                if (File.Exists(Path.Combine("Content", asFileName + ".xnb")))
                    texture = World.Instance.Game.Content.Load<Texture2D>(asFileName);

            if (texture == null)
                Log.Instance.Log("WARN: Could not find requested texture " + asFileName + "\n");

            return texture;
        }

        
    }
}
