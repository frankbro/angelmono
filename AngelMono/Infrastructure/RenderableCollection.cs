﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AngelMono.Infrastructure.Console;
using AngelMono.Infrastructure.Logging;
using AngelMono.Actors;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AngelMono.Infrastructure
{
    public class RenderableCollection
    {
        private struct RenderableLayerPair
        {
            public Renderable _renderable;
            public int _layer;

            public RenderableLayerPair(Renderable aRenderable, int aiLayer)
            {
                _renderable = aRenderable;
                _layer = aiLayer;
            }
        }

        public delegate void ActorAction(Actor a);
        public delegate void RenderableAction(Renderable r);

        public bool Show { get; set; }
        public string Name { get { return _name; } }
        public Camera Camera { get; set; }

        private string _name;
        private RenderableAction _addAction;
        private RenderableAction _removeAction;
        private bool _elementsLocked = false;
        private Dictionary<int, LinkedList<Renderable>> _layers = new Dictionary<int, LinkedList<Renderable>>();
        private LinkedList<RenderableLayerPair> _deferredAdds = new LinkedList<RenderableLayerPair>();
        private List<RenderableLayerPair> _deferredLayerChanges = new List<RenderableLayerPair>();
        private List<Renderable> _deferredRemoves = new List<Renderable>();

        public RenderableCollection(string name)
        {
            Show = true;
            _name = name;
        }

        public RenderableCollection(string name, RenderableAction addAction, RenderableAction removeAction)
        {
            Show = true;
            _name = name;
            _addAction = addAction;
            _removeAction = removeAction;
        }

        [ConsoleMethod]
        public void Add(Renderable newElement)
        {
            Add(newElement, newElement.Layer);
        }

        [ConsoleMethod]
        public void Add(Renderable newElement, int layer)
        {
            // If we're not locked, add directly to _elements.
            if (!_elementsLocked)
            {
                newElement.Layer = layer;
                if (_layers.ContainsKey(layer))
                {
                    if (_layers[layer].Contains(newElement))
                        Log.Instance.Log("[RC: {0}] Trying to add actor twice!", Name);
                    else
                        _layers[layer].AddLast(newElement);
                }
                else
                {
                    LinkedList<Renderable> list = new LinkedList<Renderable>();
                    list.AddLast(newElement);
                    _layers.Add(layer, list);
                }

                if (_addAction != null)
                    _addAction(newElement);
            }
            // If we're locked, add to _deferredAdds and we'll add the new
            // Renderable after we're done updating all the _elements.
            else
            {
                RenderableLayerPair addMe = new RenderableLayerPair(newElement, layer);
                _deferredAdds.AddLast(addMe);
            }
        }

        [ConsoleMethod]
        public void Remove(Renderable oldElement)
        {
            Remove(oldElement, false);
        }

        public void Remove(Renderable oldElement, bool isLayerChange)
        {
            if (oldElement == null)
                return;

            if (_elementsLocked)
            {
                _deferredRemoves.Add(oldElement);
                return;
            }

            // First, make sure that it isn't deferred in the _deferredAdds list.
            for (LinkedListNode<RenderableLayerPair> node = _deferredAdds.First;
                node != null; node = node.Next)
            {
                if (node.Value._renderable == oldElement)
                {
                    _deferredAdds.Remove(node);

                    if (!isLayerChange)
                        node.Value._renderable.RemovedFromWorld();

                    return;
                }
            }

            // If we didn't find it in the deferred list, find/remove it from the layers.
            bool found = false;
            // Find the layer that matches the elements layer.
            LinkedList<Renderable> layer = _layers[oldElement.Layer];
            // Found the layer (list of renderables).
            // First, make sure that it isn't deferred in the _deferredAdds list.
            for (LinkedListNode<Renderable> node = layer.First;
                node != null; node = node.Next)
            {
                // Found it.
                if (node.Value == oldElement)
                {
                    // Remove the element.
                    layer.Remove(node);

                    if (!isLayerChange && _removeAction != null)
                        _removeAction(node.Value);

                    found = true;
                    // Nothing else to do.
                    break;
                }
            }

            if (!found)
            {
                //TODO: log or error handle
                Log.Instance.Log("[RC: {0}] Remove(): Entity was not found: {1}", Name, oldElement.ToString());
            }
        }

        public Renderable FindAt(int iscreenX, int iscreenY)
        {
            int[] layerOrder = new int[_layers.Keys.Count];
            _layers.Keys.CopyTo(layerOrder, 0);
            Array.Sort(layerOrder);

            Vector2 screenClick = new Vector2(iscreenX, iscreenY);
            for (int i = 0; i < layerOrder.Length; ++i)
            {
                LinkedList<Renderable> currentLayer = _layers[layerOrder[i]];
                foreach (Renderable renderable in currentLayer)
                {
                    if (renderable.IsInside(Camera, screenClick))
                        return renderable;
                }
            }

            return null;
        }

        public void ForEachActor(ActorAction myDelegate)
        {
            int[] layerOrder = new int[_layers.Keys.Count];
            _layers.Keys.CopyTo(layerOrder, 0);
            Array.Sort(layerOrder);

            for (int i = 0; i < layerOrder.Length; ++i)
            {
                LinkedList<Renderable> currentLayer = _layers[layerOrder[i]];
                foreach (Renderable renderable in currentLayer)
                {
                    Actor theActor = renderable as Actor;
                    if (theActor != null)
                        myDelegate(theActor);
                }
            }
        }

        public void UpdateLayer(Renderable element, int newLayer)
        {
            if (element.Layer == newLayer)
                return;

            RenderableLayerPair layerChange;
            layerChange._layer = newLayer;
            layerChange._renderable = element;
            _deferredLayerChanges.Add(layerChange);
        }

        public void Update(GameTime aGameTime)
        {
            Camera.Update(aGameTime);

            _elementsLocked = true;
            int[] layerOrder = new int[_layers.Keys.Count];
                _layers.Keys.CopyTo(layerOrder, 0);
                Array.Sort(layerOrder);

                for (int i = 0; i < layerOrder.Length; ++i)
                {
                    LinkedList<Renderable> currentLayer = _layers[layerOrder[i]];
                    foreach (Renderable renderable in currentLayer)
                    {
                        renderable.Update(aGameTime);
                    }
                }
                Cleanup();
            _elementsLocked = false;

            // Now that we're done updating the list, allow any deferred Adds to be processed.
            ProcessDeferredAdds();
            ProcessDeferredLayerChanges();
            ProcessDeferredRemoves();
        }

        public void Render(GameTime aTime, GraphicsDevice aDevice, SpriteBatch aBatch)
        {
            if (!Show)
                return;

            int[] layerOrder = new int[_layers.Keys.Count];
            _layers.Keys.CopyTo(layerOrder, 0);
            Array.Sort(layerOrder);

            for (int i = 0; i < layerOrder.Length; ++i)
            {
                LinkedList<Renderable> currentLayer = _layers[layerOrder[i]];
                foreach (Renderable renderable in currentLayer)
                {
                    renderable.Render(aTime, Camera, aDevice, aBatch);
                }
            }
        }

        protected void Cleanup()
        {
            int[] layerOrder = new int[_layers.Keys.Count];
            _layers.Keys.CopyTo(layerOrder, 0);
            Array.Sort(layerOrder);

            for (int i = 0; i < layerOrder.Length; ++i)
            {
                LinkedList<Renderable> currentLayer = _layers[layerOrder[i]];
                LinkedListNode<Renderable> currentNode = currentLayer.First;
                while (currentNode != null)
                {
                    if (currentNode.Value.Destroyed)
                    {
                        LinkedListNode<Renderable> removedNode = currentNode;
                        currentNode = currentNode.Next;
                        currentLayer.Remove(removedNode);
                        removedNode.Value.RemovedFromWorld();
                    }
                    else
                        currentNode = currentNode.Next;
                }
            }
        }

        protected void ProcessDeferredAdds()
        {
            foreach (RenderableLayerPair pair in _deferredAdds)
            {
                Add(pair._renderable, pair._layer);
            }
            _deferredAdds.Clear();
        }

        protected void ProcessDeferredLayerChanges()
        {
            //TODO: use appropriate layer
            foreach (RenderableLayerPair pair in _deferredLayerChanges)
            {
                Remove(pair._renderable, true);
                Add(pair._renderable, pair._layer);
            }
            _deferredLayerChanges.Clear();
        }

        protected void ProcessDeferredRemoves()
        {
            foreach (Renderable renderable in _deferredRemoves)
            {
                Remove(renderable);
            }
            _deferredRemoves.Clear();
        }
    }
}
