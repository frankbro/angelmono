﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using AngelMono.Infrastructure.Logging;
using System.Reflection;

namespace AngelMono.Infrastructure.Console
{
    internal interface Value
    {
        object Value { get; set; }
    }

    public class ConsoleParser
    {
        #region Token Types
        private abstract class Token
        {
            public enum TokenType
            {
                Unknown,
                LiteralBool,
                LiteralNumber,
                LiteralString,
                Identifier,
                Assign,
                Add,
                Subtract,
                Divide,
                Multiply,
                BeginParen,
                EndParen,
                Comma,
                Dot,

                // Added by parser
                FunctionCall,
                PropertyGet,
                PropertySet
            }

            public TokenType Type;
            public List<Token> Children = new List<Token>();
            
            public Token(TokenType aType)
            {
                Type = aType;
            }

            public abstract Value Execute(DeveloperConsole parent);

            public virtual Value ExecuteOn(DeveloperConsole parent, Value scope)
            {
                throw new Exception(String.Format("Token {0} does not support ExecuteOn.", Type));
            }

            public static object GetValue(object token)
            {
                Value valObj = token as Value;
                if(valObj != null)
                    return valObj.Value;

                return token;
            }
        }

        private class LiteralBool : Token, Value
        {
            private bool _Value;

            public object Value
            {
                get { return _Value; }
                set { throw new Exception("Attempting to assign value to literal."); }
            }

            public LiteralBool(bool abValue)
                : base(TokenType.LiteralBool)
            {
                _Value = abValue;
            }

            public override Value Execute(DeveloperConsole parent)
            {
                return this;
            }
        }

        private class LiteralNumber : Token, Value
        {
            private float _Value;

            public object Value
            {
                get { return _Value; }
                set { throw new Exception("Attempting to assign value to literal."); }
            }

            public LiteralNumber(float afValue)
                : base(TokenType.LiteralNumber)
            {
                _Value = afValue;
            }

            public override Value Execute(DeveloperConsole parent)
            {
                return this;
            }
        }

        private class LiteralString : Token, Value
        {
            private string _Value;

            public object Value
            {
                get { return _Value; }
                set { throw new Exception("Attempting to assign value to literal."); }
            }

            public LiteralString(string asValue)
                : base(TokenType.LiteralString)
            {
                _Value = asValue;
            }

            public override Value Execute(DeveloperConsole parent)
            {
                return this;
            }
        }

        private class Identifier : Token
        {
            public string Id;

            public Identifier(string asId)
                : base(TokenType.Identifier)
            {
                Id = asId;
            }

            public override Value Execute(DeveloperConsole parent)
            {
                if (parent.CurrentScope != null)
                {
                    PropertyInfo prop = parent.ItemManager.GetPropertyInfo(parent.CurrentScope, Id);
                    if(prop != null)
                        return new PropertyValue(parent.CurrentScope, prop);
                }

                if(parent.ItemManager.IsNamespace(Id))
                    return new ConsoleNamespace(Id);

                ConsoleVariable var = parent.ItemManager.FindCVar(Id);
                if (var != null)
                    return var;

                return null;
            }

            public override Value ExecuteOn(DeveloperConsole parent, Value scope)
            {
                ConsoleNamespace ns = scope as ConsoleNamespace;
                if(ns != null)
                {
                    Type enumType = parent.ItemManager.FindEnum(ns.Id);
                    if(enumType != null)
                        return new ObjectValue(Enum.Parse(enumType, Id, false));
                }

                PropertyInfo prop = parent.ItemManager.GetPropertyInfo(scope.Value, Id);
                if(prop == null)
                    throw new Exception(String.Format("Unknown property {0} on object {1}", Id, scope));
                
                return new PropertyValue(scope.Value, prop);
            }
        }

        private class ObjectValue : Value
        {
            private object _Value;

            public object Value
            {
                get { return _Value; }
                set { _Value = value; }
            }

            public ObjectValue(object val)
            {
                _Value = val;
            }
        }

        private class PropertyValue : Value
        {
            public object On;
            public PropertyInfo Info;

            public PropertyValue(object on, PropertyInfo info)
            {
                On = on;
                Info = info;
            }

            public object Value
            {
                get 
                {
                    return Info.GetGetMethod().Invoke(On, null);
                }
                set 
                {
                    if (Info.PropertyType == typeof(int))
                        value = Convert.ToInt32(value);
                    Info.GetSetMethod().Invoke(On, new object[] { value });
                }
            }
        }

        private class ConsoleNamespace : Value
        {
            public string Id;

            public object Value
            {
                get { throw new Exception(Id + "is a namespace and not a valid value."); }
                set { throw new Exception("Cannot assign value to namespace " + Id); }
            }

            public ConsoleNamespace(string Id)
            {
                this.Id = Id;
            }
        }

        private class Operation : Token
        {
            public Operation(char acValue)
                : base(TokenType.Unknown)
            {
                switch (acValue)
                {
                    case '=': Type = TokenType.Assign; break;
                    case '+': Type = TokenType.Add; break;
                    case '-': Type = TokenType.Subtract; break;
                    case '*': Type = TokenType.Multiply; break;
                    case '/': Type = TokenType.Divide; break;
                    case '.': Type = TokenType.Dot; break;
                    case '(': Type = TokenType.BeginParen; break;
                    case ')': Type = TokenType.EndParen; break;
                    case ',': Type = TokenType.Comma; break;
                }
            }

            public override Value Execute(DeveloperConsole parent)
            {
                Value endVal = null;
                switch (Type)
                {
                    case TokenType.Assign:
                        {
                            // Evaluate RHS children, assign to identifier
                            Value lhs = Children[0].Execute(parent);
                            if (lhs == null && Children[0] is Identifier)
                                lhs = parent.ItemManager.GetCVar(((Identifier)Children[0]).Id);
                            Value rhsValue = Children[1].Execute(parent);

                            lhs.Value = rhsValue.Value;

                            endVal = lhs;
                        }
                        break;
                    case TokenType.Add:
                        {
                            float lhsValue = Convert.ToSingle(Children[0].Execute(parent).Value);
                            float rhsValue = Convert.ToSingle(Children[1].Execute(parent).Value);
                            float value = lhsValue + rhsValue;
                            endVal = new LiteralNumber(value);
                        }
                        break;
                    case TokenType.Subtract:
                        {
                            float lhsValue = Convert.ToSingle(Children[0].Execute(parent).Value);
                            float rhsValue = Convert.ToSingle(Children[1].Execute(parent).Value);
                            float value = lhsValue - rhsValue;
                            endVal = new LiteralNumber(value);
                        }
                        break;
                    case TokenType.Multiply:
                        {
                            float lhsValue = Convert.ToSingle(Children[0].Execute(parent).Value);
                            float rhsValue = Convert.ToSingle(Children[1].Execute(parent).Value);
                            float value = lhsValue * rhsValue;
                            endVal = new LiteralNumber(value);
                        }
                        break;
                    case TokenType.Divide:
                        {
                            float lhsValue = Convert.ToSingle(Children[0].Execute(parent).Value);
                            float rhsValue = Convert.ToSingle(Children[1].Execute(parent).Value);
                            float value = lhsValue / rhsValue;
                            endVal = new LiteralNumber(value);
                        }
                        break;
                    case TokenType.BeginParen:
                        endVal = Children[0].Execute(parent);
                        break;
                    case TokenType.Dot:
                        {
                            Value lhsValue = Children[0].Execute(parent);
                            endVal = Children[1].ExecuteOn(parent, lhsValue);
                        }
                        break;
                }

                return endVal;
            }

            public override Value ExecuteOn(DeveloperConsole parent, Value scope)
            {
                if (Type == TokenType.Dot)
                {
                    Value lhsValue = Children[0].ExecuteOn(parent, scope);
                    return Children[1].ExecuteOn(parent, lhsValue);
                }
                return base.ExecuteOn(parent, scope);
            }
        }

        private class FunctionCall : Token
        {
            public string Function;

            public FunctionCall(string asFunction)
                : base(TokenType.FunctionCall)
            {
                Function = asFunction;
            }

            private object[] GetParamArray(DeveloperConsole parent)
            {
                object[] parameters = new object[Children.Count];
                for (int i = 0; i < Children.Count; ++i)
                {
                    parameters[i] = Children[i].Execute(parent).Value;
                }

                return parameters;
            }

            public override Value Execute(DeveloperConsole parent)
            {
                object[] paramArray = GetParamArray(parent);
                
                ConsoleCommand cvarCommand = null;

                object currentThis = null;
                if (parent.CurrentScope != null)
                {
                    cvarCommand = parent.ItemManager.FindCommand(parent.CurrentScope, Function, false, paramArray);

                    if (cvarCommand != null)
                        currentThis = parent.CurrentScope;
                }
                
                if(cvarCommand == null)
                    cvarCommand = parent.ItemManager.FindCommand(null, Function, true, paramArray);

                if(cvarCommand == null)
                {
                    if(parent.CurrentScope != null)
                        throw new Exception(String.Format("[Console] Execute(): Could not find global function {0} (or for object {1})", Function, currentThis.ToString()));
                    else
                        throw new Exception(String.Format("[Console] Execute(): Could not find global function {0}.", Function));
                }

                return new ObjectValue(cvarCommand.Execute(parent.CurrentScope, paramArray));
            }

            public override Value ExecuteOn(DeveloperConsole parent, Value scope)
            {
                object[] paramArray = GetParamArray(parent);

                ConsoleNamespace ns = scope as ConsoleNamespace;
                if (ns != null)
                {
                    ConsoleCommand command = parent.ItemManager.FindCommand(ns.Id, Function, false, paramArray);
                    if (command == null)
                        throw new Exception(String.Format("[Console] No such method or property {0} on namespace {1}", Function, ns.Id));

                    return new ObjectValue(command.Execute(null, paramArray));
                }

                ConsoleCommand cvarCommand = parent.ItemManager.FindCommand(scope.Value, Function, false, paramArray);
                if (cvarCommand == null)
                    throw new Exception(String.Format("[Console] Execute(): Could not find function {0} for object {1}.", Function, scope));
                    
                return new ObjectValue(cvarCommand.Execute(scope.Value, paramArray));
            }
        }
        #endregion

        private static char[] s_SupportedSymbols = new char[] { '=', '+', '-', '*', '/', '.', '(', ')', ',', '{', '}' };

        private DeveloperConsole _parent;
        private int _iCurrentToken;
        private Token[] _tokens;

        public ConsoleParser(DeveloperConsole aParent)
        {
            _parent = aParent;
        }

        public object Execute(TextReader input)
        {
            object endVal = null;

            try
            {
                while (input.Peek() != -1)
                {
                    _tokens = Lex(input);
                    if (_tokens.Length != 0)
                    {
                        _iCurrentToken = 0;

                        Token root = Statement();

                        Value val = root.Execute(_parent);
                        if (val != null)
                            endVal = val.Value;

                        if (_iCurrentToken < _tokens.Length)
                            _parent.Echo("Extra input found in execution line?"); 
                    }
                }
            }
            catch (Exception e)
            {
                _parent.Echo("Error executing line: " + e.Message);
            }

            return endVal;
        }

        // This is not a praticularly complex lexer, and thus we're not using complex lexing algorithms
        // that might be faster or at least more versitale.  This one is simple to read and only really 
        // supports identifiers, numbers literals, string literals, comments as "# or ;", and the 
        // symbols =,+,-,*,/,.,(,) and ,
        // It assumes that each line is an executable piece of code.
        private static Token[] Lex(TextReader input)
        {
            List<Token> tokenList = new List<Token>();
            StringBuilder currentToken = new StringBuilder();
            string line = input.ReadLine();
            for(int i = 0; i < line.Length; )
            {
                if (Char.IsWhiteSpace(line[i]))
                {
                    for (; i < line.Length && Char.IsWhiteSpace(line[i]); ++i)
                    {
                        // Do nothing, loop through white space
                    }
                }
                else if (line[i] == '#' || line[i] == ';')
                {
                    // The rest of this line is comment.  Ignore it.
                    break;
                }
                // Check for number literals
                else if (Char.IsDigit(line[i]))
                {
                    for (; i < line.Length && Char.IsDigit(line[i]); ++i)
                    {
                        currentToken.Append(line[i]);
                    }

                    // If we're not at the end of the line
                    if (i < line.Length)
                    {
                        if (line[i] == '.')
                        {
                            // Still got a number here, it's a floating point number!
                            currentToken.Append(line[i++]);
                            for (; i < line.Length && Char.IsDigit(line[i]); ++i)
                            {
                                currentToken.Append(line[i]);
                            }
                        }

                        if (i < line.Length && Char.IsLetter(line[i]))
                        {
                            // Assure that the next character not a letter
                            throw new Exception("Unexpected letter after digit.");
                        }
                    }

                    tokenList.Add(new LiteralNumber(float.Parse(currentToken.ToString())));
                    currentToken.Remove(0, currentToken.Length);
                }
                // Check for string literals
                else if (line[i] == '\"')
                {
                    // ignore starting quote
                    i++;
                    for (; i < line.Length && line[i] != '\"'; ++i)
                    {
                        currentToken.Append(line[i]);
                    }

                    if (i >= line.Length)
                        throw new Exception("Unexpected end of line found before string closed.");

                    tokenList.Add(new LiteralString(currentToken.ToString()));
                    currentToken.Remove(0, currentToken.Length);
                    // ignore ending quote
                    i++;
                }
                else if (line[i] == '{')
                {
                    // ignore starting bracket
                    i++;
                    for (int inumOpen = 1; inumOpen > 0; ++i)
                    {
                        if (i == line.Length)
                        {

                            do
                            {
                                // Get the next line
                                currentToken.Append('\n');
                                line = input.ReadLine();
                            } while (line != null && line.Length == 0);
                            if (line == null)
                                throw new Exception("Unexpected end of input found in multi-line string literal.");
                            i = 0;
                        }

                        if (line[i] == '}')
                        {
                            inumOpen--;
                            if (inumOpen > 0)
                                currentToken.Append(line[i]);
                        }
                        else if (line[i] == '{')
                        {
                            inumOpen++;
                            currentToken.Append(line[i]);
                        }
                        else
                            currentToken.Append(line[i]);
                    }

                    tokenList.Add(new LiteralString(currentToken.ToString()));
                    currentToken.Remove(0, currentToken.Length);
                }
                // Check for identifiers (starts with any letter or underscore)
                else if (Char.IsLetter(line[i]) || line[i] == '_')
                {
                    for (; i < line.Length && (Char.IsLetterOrDigit(line[i]) || line[i] == '_'); ++i)
                    {
                        currentToken.Append(line[i]);
                    }

                    string theToken = currentToken.ToString();
                    currentToken.Remove(0, currentToken.Length);
                    // Reserved words
                    if (theToken.Equals("true", StringComparison.CurrentCultureIgnoreCase))
                        tokenList.Add(new LiteralBool(true));
                    else if (theToken.Equals("false", StringComparison.CurrentCultureIgnoreCase))
                        tokenList.Add(new LiteralBool(false));
                    else
                        tokenList.Add(new Identifier(theToken));

                }
                // All other symbols
                else if (s_SupportedSymbols.Contains(line[i]))
                {
                    tokenList.Add(new Operation(line[i++]));
                }
                else
                    throw new Exception("Unexpected symbol in input: " + line[i]);
            }

            return tokenList.ToArray();
        }


        private Token Statement()
        {
            if (_iCurrentToken >= _tokens.Length)
                throw new Exception("Unexpected end of line found!");

            Token lhs = Expression();

            // Look ahead, if the next token in an =, we have an assignment
            if (_iCurrentToken < _tokens.Length
                && _tokens[_iCurrentToken].Type == Token.TokenType.Assign)
            {
                Token assign = _tokens[_iCurrentToken];

                assign.Children.Add(lhs);
                Match(Token.TokenType.Assign);
                assign.Children.Add(Statement());

                return assign;
            }

            return lhs;
        }

        private Token Expression()
        {
            if (_iCurrentToken >= _tokens.Length)
                throw new Exception("Unexpected end of line found!");

            Token lhs = Term();

            if(_iCurrentToken < _tokens.Length)
            {
                Token currentToken = _tokens[_iCurrentToken];
                switch (currentToken.Type)
                {
                    case Token.TokenType.Add:
                    case Token.TokenType.Subtract:
                        currentToken.Children.Add(lhs);
                        Match(currentToken.Type);
                        currentToken.Children.Add(Expression());
                        return currentToken;
                }
            }

            return lhs;
        }

        private Token Term()
        {
            if (_iCurrentToken >= _tokens.Length)
                throw new Exception("Unexpected end of line found!");

            Token lhs = CompoundTerm();

            if (_iCurrentToken < _tokens.Length)
            {
                // If we can look ahead, see what this is
                Token lookahead = _tokens[_iCurrentToken];
                switch (lookahead.Type)
                {
                    case Token.TokenType.Multiply:
                    case Token.TokenType.Divide:
                        lookahead.Children.Add(lhs);
                        Match(lookahead.Type);
                        lookahead.Children.Add(Factor());
                        return lookahead;     
                }
            }

            return lhs;
        }

        private Token CompoundTerm()
        {
            if (_iCurrentToken >= _tokens.Length)
                throw new Exception("Unexpected end of line found!");

            Token lhs = Factor();

            if (_iCurrentToken < _tokens.Length)
            {
                Token lookahead = _tokens[_iCurrentToken];
                if (lookahead.Type == Token.TokenType.Dot)
                {
                    lookahead.Children.Add(lhs);
                    Match(lookahead.Type);
                    lookahead.Children.Add(CompoundTerm());
                    return lookahead;
                }
            }

            return lhs;
        }

        private Token Factor()
        {
            Token currentToken = _tokens[_iCurrentToken];
            switch(currentToken.Type)
            {
                case Token.TokenType.BeginParen:
                    Match(Token.TokenType.BeginParen);
                    currentToken.Children.Add(Expression());
                    Match(Token.TokenType.EndParen);
                    return currentToken;
                case Token.TokenType.Identifier:
                    // Check if function call
                    if (_iCurrentToken + 1 < _tokens.Length &&
                        _tokens[_iCurrentToken + 1].Type == Token.TokenType.BeginParen)
                    {
                        return Function();
                    }
                    goto default;
                case Token.TokenType.Add:
                case Token.TokenType.Subtract:
                    if (_iCurrentToken + 1 < _tokens.Length &&
                        _tokens[_iCurrentToken + 1].Type == Token.TokenType.LiteralNumber)
                    {
                        Token plusMinus = Match(currentToken.Type);
                        LiteralNumber literal = Match(Token.TokenType.LiteralNumber) as LiteralNumber;
                        if (plusMinus.Type == Token.TokenType.Subtract)
                            literal = new LiteralNumber(-Convert.ToSingle(literal.Value));

                        return literal;
                    }
                    goto default;
                case Token.TokenType.LiteralNumber:
                default:
                    return Match(currentToken.Type);
            }
        }

        private Token Function()
        {
            Identifier identifier = Match(Token.TokenType.Identifier) as Identifier;
            FunctionCall call = new FunctionCall(identifier.Id);
            Match(Token.TokenType.BeginParen);
            while (_tokens[_iCurrentToken].Type != Token.TokenType.EndParen)
            {
                call.Children.Add(Expression());
                if (_tokens[_iCurrentToken].Type != Token.TokenType.EndParen)
                    Match(Token.TokenType.Comma);
            }
            Match(Token.TokenType.EndParen);

            return call;
        }

        private Token Match(Token.TokenType type)
        {
            if (_tokens[_iCurrentToken].Type == type)
                return _tokens[_iCurrentToken++];

            throw new Exception("Invalid syntax.  Expected " + type + " found " + _tokens[_iCurrentToken].ToString());
        }
    }
}