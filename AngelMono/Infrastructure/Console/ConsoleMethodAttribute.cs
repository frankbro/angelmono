﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AngelMono.Infrastructure.Console
{
    [AttributeUsage(AttributeTargets.Method)]
    public class ConsoleMethodAttribute : Attribute
    {
        public string Name = null;
        public ConsoleCommand.Flags Flags = ConsoleCommand.Flags.None;

        public ConsoleMethodAttribute()
        {

        }

        public ConsoleMethodAttribute(string asName)
        {
            Name = asName;
        }
    }

    [AttributeUsage(AttributeTargets.Enum)]
    public class ConsoleEnumAttribute : Attribute
    {
        public ConsoleEnumAttribute()
        {

        }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class ConsolePropertyAttribute : Attribute
    {
        public string Name = null;
        public bool Serialize = true;

        public ConsolePropertyAttribute()
        {

        }

        public ConsolePropertyAttribute(string asName)
        {
            Name = asName;
        }
    }
}
