﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using AngelMono.Infrastructure.Logging;

namespace AngelMono.Infrastructure.Console
{
    public class ConsoleItemManager
    {
        private Dictionary<string, ConsoleVariable> _cvarTable = new Dictionary<string,ConsoleVariable>();

        private Dictionary<string, Dictionary<string, List<ConsoleCommand>>> _staticCommandTable = new Dictionary<string, Dictionary<string, List<ConsoleCommand>>>();
        private Dictionary<string, Dictionary<string, List<ConsoleCommand>>> _typeCommandTable = new Dictionary<string, Dictionary<string, List<ConsoleCommand>>>();
        private Dictionary<string, Dictionary<string, PropertyInfo>> _typePropertyTable = new Dictionary<string, Dictionary<string, PropertyInfo>>();

        private Dictionary<Type, ConsoleType> _typeTable = new Dictionary<Type, ConsoleType>();
        private Dictionary<string, Type> _enumTable = new Dictionary<string, Type>();

        public ConsoleItemManager()
        {
            // Global namespace always exists
            _staticCommandTable.Add("", new Dictionary<string, List<ConsoleCommand>>());
        }

        public ConsoleVariable GetCVar( string id )
        {
            ConsoleVariable cvar = GetCVarInternal(id);

            if (cvar != null)
                return cvar;

            return CreateConsoleVariable(id);
        }

        public ConsoleVariable FindCVar(string id)
        {
            return GetCVarInternal(id);
        }

        public Type FindEnum(string name)
        {
            if(_enumTable.ContainsKey(name))
                return _enumTable[name];

            return null;
        }

        public bool IsNamespace(string value)
        {
            return _enumTable.ContainsKey(value) || _staticCommandTable.ContainsKey(value);
        }

        public ConsoleCommand FindCommand(object scope, string command, bool allowGlobal, object[] parameters)
        {
            Dictionary<string, List<ConsoleCommand>> commandScope = null;

            if (scope is string)
            {
                if (_staticCommandTable.ContainsKey((string)scope))
                    commandScope = _staticCommandTable[(string)scope];
            }
            else if (scope == null)
                commandScope = _staticCommandTable[""];
            else
            {
                if (_typeCommandTable.ContainsKey(scope.GetType().FullName))
                    commandScope = _typeCommandTable[scope.GetType().FullName];
            }

            if (commandScope == null)
                return null;

            ConsoleCommand retCommand = null;
            if (commandScope != null && commandScope.ContainsKey(command))
                retCommand = FindBestMatch(commandScope[command], parameters);

            // If we were searching non-global but we didn't find a command
            // and we're allowing global calls to be made...
            if (commandScope == null && retCommand == null && allowGlobal)
            {
                // Search for the global call
                if (_staticCommandTable[""].ContainsKey(command))
                    retCommand = FindBestMatch(_staticCommandTable[""][command], parameters);
            }

            return retCommand;
        }

        public void LoadMembersFromAssembly(Assembly assm)
        {
#if WINDOWS
            // Don't bother with GAC entries
            if (assm.GlobalAssemblyCache == true)
                return;
#endif
            foreach (Type type in assm.GetTypes())
            {
                string sshortTypeName = type.Name;
                string stypeName = type.FullName;
                //if (type.IsGenericType || type.IsGenericTypeDefinition || type.IsInterface || type.BaseType.IsGenericType)
				if (type.IsGenericType || type.IsGenericTypeDefinition || type.IsInterface || (type.BaseType != null && type.BaseType.IsGenericType))
                    continue;

                if (type.IsEnum)
                {
                    ConsoleEnumAttribute[] attribute = (ConsoleEnumAttribute[])type.GetCustomAttributes(typeof(ConsoleEnumAttribute), true);
                    if (attribute != null && attribute.Length > 0)
                        _enumTable.Add(type.Name, type);
                }
                
                MemberInfo[] info = type.GetMembers(BindingFlags.Instance | BindingFlags.Public | BindingFlags.Static);

                for (int i = 0; i < info.Length; ++i)
                {
                    if (info[i].MemberType == MemberTypes.Method)
                    {
                        ConsoleMethodAttribute[] method = (ConsoleMethodAttribute[])info[i].GetCustomAttributes(typeof(ConsoleMethodAttribute), true);
                        if (method != null && method.Length > 0)
                        {
                            string name = method[0].Name;
                            if (name == null)
                                name = info[i].Name;

                            ConsoleCommand newCommand = new ConsoleCommand(name, (MethodInfo)info[i]);
                            newCommand.SetFlag((int)method[0].Flags);

                            MethodInfo myinfo = (MethodInfo)info[i];
                            Dictionary<string, List<ConsoleCommand>> commandMap;
                            if(myinfo.IsStatic)
                            {
                                if(_staticCommandTable.ContainsKey(sshortTypeName))
                                    commandMap = _staticCommandTable[sshortTypeName];
                                else
                                {
                                    commandMap = new Dictionary<string,List<ConsoleCommand>>();
                                    _staticCommandTable.Add(sshortTypeName, commandMap);
                                }
                            }
                            else
                            {
                                if(_typeCommandTable.ContainsKey(stypeName))
                                    commandMap = _typeCommandTable[stypeName];
                                else
                                {
                                    commandMap = new Dictionary<string,List<ConsoleCommand>>();
                                    _typeCommandTable.Add(stypeName, commandMap);
                                }
                            }

                            if(commandMap.ContainsKey(name))
                                commandMap[name].Add(newCommand);
                            else
                            {
                                List<ConsoleCommand> newList = new List<ConsoleCommand>();
                                newList.Add(newCommand);
                                commandMap.Add(name, newList);
                            }
                        }
                    }
                    else if (info[i].MemberType == MemberTypes.Property)
                    {
                        ConsolePropertyAttribute[] method = (ConsolePropertyAttribute[])info[i].GetCustomAttributes(typeof(ConsolePropertyAttribute), true);
                        if (method != null && method.Length > 0)
                        {
                            string name = method[0].Name;
                            if (name == null)
                                name = info[i].Name;

                            PropertyInfo myinfo = (PropertyInfo)info[i];
                            if (!_typePropertyTable.ContainsKey(stypeName))
                                _typePropertyTable.Add(stypeName, new Dictionary<string, PropertyInfo>());

                            if (_typePropertyTable.ContainsKey(name))
                                _typePropertyTable[stypeName][name] = myinfo;
                            else
                                _typePropertyTable[stypeName].Add(name, myinfo);
                        }
                    }
                }
            }
        }

        public void AddCommand(string id, ConsoleCommandHandler handler)
        {
            ConsoleCommand newCommand = new ConsoleCommand(id, handler);
            //newCommand.SetFlag( (int)flags );

            if (_staticCommandTable[""].ContainsKey(id))
            {
                Log.Instance.Log("WARN: Replacing global console command: " + id);
                _staticCommandTable[""][id][0] = newCommand;
            }
            else
            {
                List<ConsoleCommand> commandList = new List<ConsoleCommand>();
                commandList.Add(newCommand);
                _staticCommandTable[""].Add(id, commandList);
            }
        }

        public void RemoveCommand(string id)
        {
            if (_staticCommandTable[""].ContainsKey(id))
            {
                _staticCommandTable.Remove(id);
            }
            else
                Log.Instance.Log("WARN: Attempting to remove non-existant global function {0}", id);
        }

        public void AddType(ConsoleType type)
        {
            if (_typeTable.ContainsKey(type.RealType))
                _typeTable[type.RealType] = type;
            else
                _typeTable.Add(type.RealType, type);

            // Add the console command for deserializing
            AddCommand(type.ConsoleName, type._deserializer);
        }

        public ConsoleType GetConsoleType(Type realType)
        {
            if (_typeTable.ContainsKey(realType))
                return _typeTable[realType];

            return null;
        }

        public PropertyInfo GetPropertyInfo(object obj, string id)
        {
            if(!_typePropertyTable.ContainsKey(obj.GetType().FullName))
                return null;

            Dictionary<string, PropertyInfo> props = _typePropertyTable[obj.GetType().FullName];
            if (!props.ContainsKey(id))
                return null;


            return props[id];
        }

        public void GetConsoleItemIds(string againstSubstring, ref List<string> out_ids )
        {
            if (againstSubstring.Length == 0)
                return;

            if (out_ids == null)
                out_ids = new List<string>();

            GetConsoleCommandIds(againstSubstring, ref out_ids);
            GetConsoleVariableIds(againstSubstring, ref out_ids);
            GetConsoleNamespaces(againstSubstring, ref out_ids);
        }

        public void GetConsoleNamespaces(string againstSubstring, ref List<string> out_ids)
        {
            if (out_ids == null)
                out_ids = new List<string>();

            foreach (string s in _enumTable.Keys)
            {
                if(s.StartsWith(againstSubstring))
                    out_ids.Add(s);
            }

            foreach (string s in _staticCommandTable.Keys)
            {
                // Ignore anything with a dot.
                if (!s.Contains('.') && s.StartsWith(againstSubstring))
                    out_ids.Add(s);
            }
        }

        public void GetConsoleVariableIds(string againstSubstring, ref List<string> out_ids)
        {
            if(out_ids == null)
                out_ids = new List<string>();

	        foreach(ConsoleItem item in _cvarTable.Values)
	        {
                TryWriteConsoleItemsToList(item, againstSubstring, ref out_ids);
	        }
        }

        public void GetConsoleCommandIds(string againstSubstring, ref List<string> out_ids )
        {
            if(out_ids == null)
                out_ids = new List<string>();

            //check against cmdTable
            foreach(List<ConsoleCommand> commandList in _staticCommandTable[""].Values)
            {
                foreach (ConsoleCommand item in commandList)
	            {
                    TryWriteConsoleItemsToList(item, againstSubstring, ref out_ids);
	            }
            }
        }

        public void SerializeConfigCVars(List<string> configCVars)
        {
            //check against cvarTable
	        foreach( ConsoleVariable cvar in _cvarTable.Values )
	        {
		        if( cvar != null && cvar.HasFlag((int)ConsoleVariable.Flags.Config) )
		        {
                    configCVars.Add(cvar.Serialize());
		        }
	        }
        }

        private ConsoleVariable GetCVarInternal(string useId)
        {
            if (_cvarTable.ContainsKey(useId))
                return _cvarTable[useId];
            else
                return null;
        }

        private ConsoleVariable CreateConsoleVariable(string useId)
        {
            ConsoleVariable newVar = new ConsoleVariable(useId);
            //Add to map
            _cvarTable.Add(useId, newVar);

            return newVar;
        }

        private void TryWriteConsoleItemsToList( ConsoleItem item, string againstSubstring, ref List<string> out_ids )
        {
	        if( item != null && !item.IgnoreForAutoComplete)
	        {
		        string id = item.Id.Substring(0, Math.Min(againstSubstring.Length, item.Id.Length) );
		        if( againstSubstring == id )
		        {
			        string compareText = item.Id;
			        string insertText = item.Description;
			        //Insert sorted
			        for( int i = 0; i < out_ids.Count; ++i) 
			        {
				        string val = out_ids[i];
				        if( compareText.CompareTo(val) < 0  )
				        {
					        out_ids.Insert(i, insertText );
					        return;
				        }
			        }
			        //otherwise, push it on the back
			        out_ids.Add( insertText );
		        }
	        }
        }

        private ConsoleCommand FindBestMatch(List<ConsoleCommand> possibleCommands, object[] parameters)
        {
            ConsoleCommand bestMatch = null;
            foreach (ConsoleCommand possibleMatch in possibleCommands)
            {
                ConsoleCommand.ParamMatch currentMatch = possibleMatch.DoParametersMatch(parameters);
                if (currentMatch == ConsoleCommand.ParamMatch.Exact)
                {
                    bestMatch = possibleMatch;
                    break;
                }
                else if (currentMatch == ConsoleCommand.ParamMatch.Close)
                    bestMatch = possibleMatch;
            }

            return bestMatch;
        }
    }
}
