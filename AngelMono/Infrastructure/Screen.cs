﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AngelMono.Infrastructure
{
    public class Screen
    {
        protected List<Renderable> _WorldObjects = new List<Renderable>();
        protected List<Renderable> _UIObjects = new List<Renderable>();

        public void AddToScreen(Renderable newObject)
        {
            World.Instance.Add(newObject);
            _WorldObjects.Add(newObject);
        }

        public void AddToScreenUI(Renderable newObject)
        {
            UI.Instance.Add(newObject);
            _UIObjects.Add(newObject);
        }

        public virtual void Enter() { }

        public virtual void Exit()
        {
            foreach (Renderable obj in _WorldObjects)
            {
                World.Instance.Remove(obj);
            }
            _WorldObjects.Clear();

            foreach (Renderable obj in _UIObjects)
            {
                UI.Instance.Remove(obj);
            }
            _UIObjects.Clear();
        }

        public virtual void Update(GameTime time) { }
        public virtual void Render(GameTime time, Camera camera, GraphicsDevice device, SpriteBatch batch) { }
    }
}
