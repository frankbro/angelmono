﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using AngelMono.Physics;
using AngelMono.AI;
using Microsoft.Xna.Framework.Graphics;
using AngelMono.Infrastructure.Console;
using AngelMono.Infrastructure.Logging;
using AngelMono.Messaging;
using AngelMono.Actors;

//using Box2D.XNA;
using FarseerPhysics;
using FarseerPhysics.Dynamics;
using System.IO;
using System.Threading;

#if WINDOWS
using AngelMono.Editor;
using System.Windows.Forms;
#endif

namespace AngelMono.Infrastructure
{
    //public delegate void CollisionHandler(Geom geom1, Geom geom2, float normalImpulse, Vector2 hitLocation);

    public class World
    {
        private static World s_Instance = new World();

        private bool _simulateOn = true;
        private bool _initialized = false;
        
        // Threaded physics
        private bool _physicsSetUp = false;
        //private Box2D.XNA.World _physicsWorld;
		private FarseerPhysics.Dynamics.World _physicsWorld;
        private volatile bool _bThreadedPhysics = true;
        private Thread _physicsThread;
        private float _lastDt;
        private AutoResetEvent _runPhysics = new AutoResetEvent(false);
        private AutoResetEvent _waitPhysics = new AutoResetEvent(true);

        private RenderableCollection _renderables = new RenderableCollection("World", 
            r => r.AddedToWorld(), r => r.RemovedFromWorld());

        private Game _game;
        private GameManager _gameManager;
        private CollisionManager _collisionManager = new CollisionManager();

#if WINDOWS
        private EditorForm _editor;

        public bool IsInEditMode
        {
            get { return _editor != null; }
        }

        public EditorForm Editor
        {
            get { return _editor; }
        }
#endif

        public static World Instance
        {
            get { return s_Instance; }
        }

        //public Box2D.XNA.World PhysicsWorld
		public FarseerPhysics.Dynamics.World PhysicsWorld
        {
            get { return _physicsWorld; }
        }

        public Level CurrentLevel
        {
            get;
            set;
        }

        public Game Game
        {
            get { return _game; }
        }

        public GameManager GameManager
        {
            get { return _gameManager; }
            set
            {
                if (_gameManager != null || value == null)
                    throw new ArgumentException("Resetting GameManager is not supported!");

                _gameManager = value;
            }
        }

        [ConsoleProperty]
        public Camera Camera
        {
            get { return _renderables.Camera; }
            set { _renderables.Camera = value; }
        }

        public bool IsSimulating
        {
            get { return _simulateOn; }
            set { _simulateOn = value; }
        }

        protected World()
        {
            CurrentLevel = new Level();
        }

        public bool Initialize(Game game)
        {
            return Initialize(game, 1024, 768, "AngelMono Engine");
        }

        public bool Initialize(Game game, int windowWidth, int windowHeight, string windowName)
        {
            if (_initialized)
                return false;

            _game = game;

            Camera = new Camera(windowWidth, windowHeight, new Vector3(0, 0, 10), new Vector3(0, 0, -10));
            UI.Instance.Camera = new Camera(windowWidth, windowHeight, new Vector3(0, 0, 10), new Vector3(0, 0, -10));

            if (File.Exists(string.Format("Config\\{0}.cfg", "autoexec")))
            {
                DeveloperConsole.Instance.ExecConfigFile("autoexec");
            }

            ConsoleVariable var = DeveloperConsole.Instance.ItemManager.GetCVar("theWorld");
            var.Value = this;
            
#if WINDOWS
            DeveloperConsole.Instance.ItemManager.AddCommand("BeginEditing", x => { BeginEditing(); return null; });
            DeveloperConsole.Instance.ItemManager.AddCommand("EndEditing", x => { EndEditing(); return null; });
            DeveloperConsole.Instance.ItemManager.AddCommand("ToggleEditing", x => { ToggleEditing(); return null; });

            Switchboard.Instance["BeginEditing"] = new MessageHandler(x => BeginEditing());
            Switchboard.Instance["ToggleEditing"] = new MessageHandler(x => ToggleEditing());
#endif
            _initialized = true;
            return true;
        }

#if WINDOWS
        public void ToggleEditing()
        {
            if (IsInEditMode)
                EndEditing();
            else
                BeginEditing();
        }

        public void BeginEditing()
        {
            if (_editor == null)
            {
                _simulateOn = false;
                _editor = new EditorForm();
                _editor.Show(Form.FromChildHandle(_game.Window.Handle));
            }
        }

        public void EndEditing()
        {
            // Regardless of the Form's status, start simulating again
            _simulateOn = true;

            if (_editor != null)
            {
                // If EndEditing was called by the form closing, don't try to call it again.
                if (!_editor.Disposing)
                    _editor.Close();
                else
                {
                    // The editor doesn't broadcast this, so the world should
                    Switchboard.Instance.Broadcast("EndEditing");
                }
                _editor = null;
            }
        }
#endif

        public void TearDown()
        {
            if (_bThreadedPhysics)
            {
                _bThreadedPhysics = false;
                _runPhysics.Set();
            }
        }

        public bool SetupPhysics()
        {
            return SetupPhysics(new Vector2(0, -10));
        }

        public bool SetupPhysics(Vector2 gravity)
        {
            if (_physicsSetUp)
                return false;

            //_physicsWorld = new Box2D.XNA.World(gravity, true);
			_physicsWorld = new FarseerPhysics.Dynamics.World(gravity);

            if (_bThreadedPhysics)
            {
                _physicsThread = new Thread(new ThreadStart(
                    delegate()
                    {
#if XBOX
                        Thread.CurrentThread.SetProcessorAffinity(4);
#endif
                        while (_bThreadedPhysics)
                        {
                            _runPhysics.WaitOne();
                            if (_bThreadedPhysics)  // If this is now false, we're shutting down
                            {
                                RunPhysics(_lastDt);
                                _waitPhysics.Set();
                            }
                        }
                    }
                ));
                _physicsThread.Start();
            }

            // TODO: Physics debug draw.
            //_physicsWorld->SetListener(this);

            //_physicsDebugDraw = new /*Physics*/DebugDraw();
            //_physicsWorld->SetDebugDraw(_physicsDebugDraw);

            //_physicsWorld.ContactListener = _collisionManager;
			_physicsWorld.ContactManager.BeginContact = new BeginContactDelegate(_collisionManager.BeginContact);
			
            _physicsSetUp = true;
            return true;
        }

        [ConsoleMethod]
        public void Add(Renderable newElement)
        {
            Add(newElement, newElement.Layer);
        }

        public void Add(Renderable newElement, int layer)
        {
            _renderables.Add(newElement, layer);
        }

        [ConsoleMethod]
        public void Remove(Renderable oldElement)
        {
            Remove(oldElement, false);
        }

        private void Remove(Renderable oldElement, bool isLayerChange)
        {
            _renderables.Remove(oldElement, isLayerChange);
        }

        public Renderable FindAt(int iscreenX, int iscreenY)
        {
            return _renderables.FindAt(iscreenX, iscreenY);
        }

        public void UpdateLayer(Renderable r, int newLayer)
        {
            _renderables.UpdateLayer(r, newLayer);
        }

        public void Simulate(GameTime aGameTime)
        {
            if (_physicsSetUp && _simulateOn && _bThreadedPhysics)
            {
                _waitPhysics.WaitOne();
                SyncPhyiscs();
            }

            // system updates
            DeveloperConsole.Instance.Update( aGameTime );
            // theControllerManager.UpdateState();

            if (_gameManager != null)
                _gameManager.Update(aGameTime);

            if (_simulateOn)
            {
                // Deliver any messages that have been queued from the last frame. 
                Switchboard.Instance.SendAllMessages();

                if (_bThreadedPhysics)
                {
                    _lastDt = (float)aGameTime.ElapsedGameTime.TotalSeconds;
                    _runPhysics.Set();
                }

                _renderables.Update(aGameTime);

                if (_physicsSetUp && !_bThreadedPhysics)
                {
                    RunPhysics((float)aGameTime.ElapsedGameTime.TotalSeconds);
                    SyncPhyiscs();
                }

                Switchboard.Instance.Update(aGameTime);
                //if there are any system updates that still need to be run, put them here
            }
        }

        
        public void ForEachActor(RenderableCollection.ActorAction myDelegate)
        {
            _renderables.ForEachActor(myDelegate);
        }

        public void Render(GameTime aTime, GraphicsDevice aDevice, SpriteBatch aBatch)
        {
            _renderables.Render(aTime, aDevice, aBatch);

            if (_gameManager != null)
                _gameManager.Render(aTime, Camera, aDevice, aBatch);

            //Render debug information
            SpatialGraphManager.Instance.Render(Camera, aDevice, aBatch);

            //Draw developer console
            DeveloperConsole.Instance.Render(Camera, aDevice, aBatch);
        }

        public void RunPhysics(float lastDt)
        {
            if (!_physicsSetUp)
                return;

            // more iterations -> more stability, more cpu
            // tune to your liking...
            //_physicsWorld.Step(lastDt, 8, 6);
			_physicsWorld.Step(lastDt);
            
        }

        private void SyncPhyiscs()
        {
            //for(Body b = _physicsWorld.GetBodyList(); b != null; b = b.GetNext())
			foreach(Body b in _physicsWorld.BodyList)
            {
                PhysicsActor physActor = b.UserData as PhysicsActor;
                if (physActor != null)
                    physActor.SyncPosRot();
            }
        }
    }
}
