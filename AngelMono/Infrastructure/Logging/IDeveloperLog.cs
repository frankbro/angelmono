﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AngelMono.Infrastructure.Logging
{
    public interface IDeveloperLog
    {
        void Log(string val);
    }
}
