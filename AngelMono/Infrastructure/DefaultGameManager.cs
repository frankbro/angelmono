﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AngelMono.Infrastructure
{
    public class DefaultGameManager : GameManager
    {
        Screen _CurrentScreen;

        public Screen CurrentScreen { get { return _CurrentScreen; } }

        public void ChangeScreen(Screen newScreen)
        {
            if (_CurrentScreen != null)
                _CurrentScreen.Exit();

            _CurrentScreen = newScreen;
            if (_CurrentScreen != null)
                _CurrentScreen.Enter();
        }

        public override void Update(GameTime aTime)
        {
            base.Update(aTime);

            if (_CurrentScreen != null)
                _CurrentScreen.Update(aTime);
        }

        public override void Render(GameTime aTime, Camera aCamera, GraphicsDevice aDevice, SpriteBatch aBatch)
        {
            base.Render(aTime, aCamera, aDevice, aBatch);

            if (_CurrentScreen != null)
                _CurrentScreen.Render(aTime, aCamera, aDevice, aBatch);
        }
    }
}
