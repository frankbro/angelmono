﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AngelMono.Actors;
using AngelMono.Infrastructure.Logging;

namespace AngelMono.Infrastructure
{
    public class Level
    {
        private string _sLevelName;
        private List<Actor> _Actors = new List<Actor>();

        public string LevelFile 
        { 
            get { return _sLevelName; }
        }

        public Level()
        {

        }

        public void AddActor(Actor a)
        {
            World.Instance.Add(a);
            _Actors.Add(a);
        }

        public void RemoveActor(Actor a)
        {
            World.Instance.Remove(a);
            _Actors.Remove(a);
        }

        public void Clear()
        {
            foreach (Renderable obj in _Actors)
            {
                World.Instance.Remove(obj);
            }
            _Actors.Clear();
            _sLevelName = null;
        }

        public void Load(string asLevelFile)
        {
            Clear();
            _sLevelName = asLevelFile;
            ActorFactory.Instance.LoadLevel(asLevelFile);

            Actor[] levelSpawnedActors = TagCollection.Instance.GetObjectsTagged("level_spawned");
            if (levelSpawnedActors != null)
            {
                foreach (Actor spawned in levelSpawnedActors)
                {
                    _Actors.Add(spawned);
                    spawned.Untag("level_spawned");
                }
            }
        }

        public void Save()
        {
            if (_sLevelName != null)
                ActorFactory.Instance.WriteLevel(_sLevelName);
            else
                Log.Instance.Log("Could not save level, no file name provided!");
        }

        public void SaveAs(string sLevelFile)
        {
            if (sLevelFile == null)
                throw new ArgumentException("sLevelFile can not be null");

            ActorFactory.Instance.WriteLevel(sLevelFile);
            _sLevelName = sLevelFile;
        }

        public static Level LoadLevel(string asLevelFile)
        {
            Level l = new Level();
            l.Load(asLevelFile);

            return l;
        }
    }
}
