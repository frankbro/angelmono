﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using Box2D.XNA;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using FarseerPhysics.Collision;
using AngelMono.Physics;
using AngelMono.Actors;
using AngelMono.Messaging;
using AngelMono.Infrastructure.Logging;

namespace AngelMono.Infrastructure
{
    /// <summary>
    /// This class is for internal use by the world to manage contacts
    /// occuring in the physics system and pushing info around for said 
    /// collisions
    /// </summary>
    //internal class CollisionManager : IContactListener
	internal class CollisionManager
    {
        #region IContactListener Members

        public bool BeginContact(Contact contact)
        {
			
            //PhysicsActor pa1 = contact.GetFixtureA().GetBody().GetUserData() as PhysicsActor;
			PhysicsActor pa1 = contact.FixtureA.Body.UserData as PhysicsActor;
            //PhysicsActor pa2 = contact.GetFixtureB().GetBody().GetUserData() as PhysicsActor;
			PhysicsActor pa2 = contact.FixtureB.Body.UserData as PhysicsActor;
            if(pa1 == null || pa2 == null)
                return false;

            pa1.OnCollision(pa2, contact);
            if (Switchboard.Instance["CollisionWith" + pa1.Name] != null)
            {
                Switchboard.Instance.Broadcast(new Message("CollisionWith" + pa1.Name));
            }

            pa2.OnCollision(pa1, contact);
            if (Switchboard.Instance["CollisionWith" + pa2.Name] != null)
            {
                Switchboard.Instance.Broadcast(new Message("CollisionWith" + pa2.Name));
            }
			return true;
        }

        public void EndContact(Contact contact)
        {
            
        }

        public void PostSolve(Contact contact, ref ContactConstraint constraint)
        {
            
        }

        public void PreSolve(Contact contact, ref Manifold oldManifold)
        {
            
        }

        #endregion
    }
}
