﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AngelMono.Infrastructure.Console;

namespace AngelMono.Infrastructure
{
    public class UI : RenderableCollection
    {
        private static UI s_Instance = new UI();

        public static UI Instance
        {
            get { return s_Instance; }
        }

        protected UI() :
            base("UI")
        {
            ConsoleVariable cvar = DeveloperConsole.Instance.ItemManager.GetCVar("theUI");
            cvar.Value = this;
        }
    }
}
