﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace AngelMono.Input
{
    public interface IMouseListener
    {
        void MouseDownEvent(Vector2 screenCoordinates, InputManager.MouseButton button);

        void MouseUpEvent(Vector2 screenCoordinates, InputManager.MouseButton button);

        void MouseWheelEvent(int amount);

        void MouseMotionEvent(int screenPosX, int screenPosY);
    }
}
