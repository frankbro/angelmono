﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AngelMono.Infrastructure.Console;
using AngelMono.Messaging;

namespace AngelMono.Input
{
    public class InputBinding
    {
        // The console message that will be executed when the corresponding input is pressed
        public string PressMessage { get; set; }
        
        // The console message that will be executed when the corresponding input is released
        public string ReleaseMessage { get; set; }

        public void OnPress(int playerNumber)
        {
            if (PressMessage == null)
            {
                return;
            }

            string messageName = PressMessage;
            string replaceString = String.Format("{0:0}", playerNumber);
            if (playerNumber == -1)
                replaceString = "";
            messageName = messageName.Replace("#", replaceString);

            //DeveloperConsole.Instance.ExecuteInConsole(messageName);
            Switchboard.Instance.Broadcast(new Message(messageName));
        }

        public void OnRelease(int playerNumber)
        {
            if (ReleaseMessage == null)
            {
                return;
            }

            string messageName = ReleaseMessage;
            string replaceString = String.Format("{0:0}", playerNumber);
            if (playerNumber == -1)
                replaceString = "";
            messageName = messageName.Replace("#", replaceString);

            //DeveloperConsole.Instance.ExecuteInConsole(messageName);
            Switchboard.Instance.Broadcast(new Message(messageName));
        }
    }
}
