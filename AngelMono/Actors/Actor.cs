﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using AngelMono.Infrastructure;
using AngelMono.Util;
using Microsoft.Xna.Framework.Content;
using AngelMono.Infrastructure.Console;
using AngelMono.Messaging;
using AngelMono.Infrastructure.Intervals;
using AngelMono.Infrastructure.Logging;
using System.ComponentModel;
using System.IO;
using AngelMono.Rendering;
using AngelMono.Editor;

#if WINDOWS
using System.Drawing.Design;
using System.ComponentModel.Design;
#endif

namespace AngelMono.Actors
{
    public class Actor : Renderable
    {
        static string[] c_StringSplit = new string[] { ", " };

        private static Dictionary<string, Actor> s_NameList = new Dictionary<string,Actor>();
        
        // Rendering 
        protected IRenderPath _renderPath;
        private string _spritePath;

        private string _name;
        private string _actorDefinition;
        private List<string> _tags = new List<string>();
        
        // Intervals; used for simple animations
        private Vector2Interval _positionInterval; string _positionIntervalMessage;
        private FloatInterval _rotationInterval; string _rotationIntervalMessage;
        private Vector2Interval _sizeInterval; string _sizeIntervalMessage;
        private ColorInterval _colorInterval; string _colorIntervalMessage;

        [ConsoleProperty]
        [Editor(typeof(XnaColorEditor), typeof(UITypeEditor))]
        public Color Color { get; set; }

        [ConsoleProperty]
        public virtual Vector2 Size { get; set; }

        [ConsoleProperty]
        public override Vector2 Position { get; set; }

        [ConsoleProperty]
        public virtual float Rotation { get; set; }

        [ConsoleProperty]
        [TypeConverter(typeof(ContentConverter))]
        public virtual string Sprite
        {
            get { return _spritePath; }
            set
            {
                SetSprite(value);
            }
        }

        [ConsoleProperty(Serialize=false)]
        [Editor(typeof(AllOfTypeObjectSelector<IRenderPath>), typeof(UITypeEditor))]
        [Category("Advanced")]
        public virtual IRenderPath RenderPath
        {
            get { return _renderPath; }
            set { _renderPath = value; }
        }

        [Editor(typeof(ActorTagEditor), typeof(UITypeEditor))]
        public string[] Tags
        {
            get { return _tags.ToArray(); }
        }
        
        public virtual BoundingBox2D Bounds
        {
            get 
            {
                return new BoundingBox2D(
                    Position - Size / 2,
                    Position + Size / 2);
            }
        }
        
        public string ActorDefinition
        {
            get { return _actorDefinition; }
            internal set { _actorDefinition = value; }
        }

        public bool IgnoreForSerialization { get; set; }

        [ConsoleProperty]
        public string Name
        {
            get { return _name; }
            set
            {
                // This function gets called a bunch during Actor Initialization when loading levels
                if (value == null || value.Length == 0)
                    value = "Actor";

                if (_name != null)
                {
                    if (Actor.GetNamed(_name) == this)
                    {
                        if (value.Equals(_name))
                            return; // No change necessary since we are just reassigning this back to its old name
                        else
                            s_NameList.Remove(_name);   // Remove the old link since this should only be linked to one name value at a time
                    }
                    _name = null;
                }

                if (Actor.GetNamed(value) == null)
                {
                    _name = value;
                }
                else
                {
                    string baseName = value;
                    char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
                    baseName = baseName.TrimEnd(digits);
                    int icounter = 1;
                    string iteratedName = baseName;
                    while (Actor.GetNamed(iteratedName) != null)
                    {
                        iteratedName = baseName + icounter++;
                    };

                    _name = iteratedName;
                }

                s_NameList.Add(_name, this);
            }
        }

        public Actor()
        {
            Color = Color.White;
            Size = Vector2.One;
            Rotation = 0.0f;
            Position = Vector2.Zero;
            Name = "";

            _renderPath = new DefaultActorRenderPath(this);
        }

        public bool IsTagged(string asTag)
        {
            return _tags.Contains(asTag.ToLower());
        }

        [ConsoleMethod]
        public void Tag(string asTag)
        {
#if XBOX360
            string[] tags = asTag.Split(',');
#else
            string[] tags = asTag.Split(c_StringSplit, StringSplitOptions.RemoveEmptyEntries);
#endif
            for (int i = 0; i < tags.Length; ++i)
            {
                tags[i] = tags[i].ToLower();
                if (!_tags.Contains(tags[i]))
                {
                    _tags.Add(tags[i]);
                    TagCollection.Instance.AddObjectToTagList(this, tags[i]);
                }
            }
        }

        [ConsoleMethod]
        public void Untag(string asOldTag)
        {
            _tags.Remove(asOldTag);
            TagCollection.Instance.RemoveObjectFromTagList(this, asOldTag);
        }

        [ConsoleMethod]
        public void ClearTags()
        {
            foreach (string tag in _tags)
            {
                TagCollection.Instance.RemoveObjectFromTagList(this, tag);
            }
            _tags.Clear();
        }

        public string[] GetTags()
        {
            return _tags.ToArray();
        }

        /// <summary>
        /// When an actor is removed from the world it loses all tags...
        /// Is this what we want? 
        /// </summary>
        public override void RemovedFromWorld()
        {
            for (int i = 0; i < _tags.Count; ++i)
            {
                TagCollection.Instance.RemoveObjectFromTagList(this, _tags[i]);
            }
            _tags.Clear();

            base.RemovedFromWorld();
        }

        [ConsoleMethod]
        public void SetLayerName(string layerName)
        {
            string cvarName = "layer_" + layerName;
            ConsoleVariable cvar = DeveloperConsole.Instance.ItemManager.FindCVar(cvarName);
            if (cvar != null)
            {
                Layer = Convert.ToInt32(cvar.Value);
            }
        }

        public RPType GetRenderPath<RPType>() 
            where RPType : class, IRenderPath
        {
            RPType path = _renderPath as RPType;
            if (_renderPath == null)
            {
                Log.Instance.Log(string.Format("GetRenderPath: Current render path is not {0}.  Is {1}\n.",
                    typeof(RPType).ToString(), _renderPath.GetType().ToString()));
            }
            return path;
        }

        [ConsoleMethod]
        public void ClearRenderPath()
        {
            RenderPath = new DefaultActorRenderPath(this);
        }

        [ConsoleMethod]
        public void SetSprite(string asFileName)
        {
            SetSprite(asFileName, false);
        }

        [ConsoleMethod]
        public void SetSprite(string asFileName, bool isStatic)
        {
            _spritePath = asFileName;
            bool hasTileInfo = TileInfoCache.HasTileInfo(asFileName);
            if (hasTileInfo)
            {
                if (isStatic || !TileInfoCache.Instance.GetForFile(asFileName).IsAnim)
                {
                    RenderPath = new TiledSpriteRenderPath(this, asFileName);
                }
                else
                {
                    TiledAnimRenderPath path = new TiledAnimRenderPath(this, asFileName);
                    path.PlayAnimation();
                    RenderPath = path;
                }
            }
            else
            {
                if (!isStatic && AnimSpriteRenderPath.IsAnimFile(asFileName))
                    RenderPath = new AnimSpriteRenderPath(this, asFileName);
                else
                    RenderPath = new StaticSpriteRenderPath(this, asFileName);
            }
        }

        [ConsoleMethod]
        public void SetAnimationDefinition(string asAnimatinoDef)
        {
            RenderPath = new MultiAnimRenderPath(this, asAnimatinoDef);
        }

        [ConsoleMethod]
        public void PlayAnimation(string name)
        {
            AnimRenderPath animPath = GetRenderPath<AnimRenderPath>();
            if (animPath != null)
                animPath.PlayAnimation(name);
        }

        [ConsoleMethod]
        public void PlayAnimation(float afDelay, AnimationType aeType)
        {
            AnimRenderPath animPath = GetRenderPath<AnimRenderPath>();
            if (animPath != null)
                animPath.PlayAnimation(afDelay, aeType);
        }

        [ConsoleMethod]
        public void PlayAnimation(float afDelay, AnimationType aeType, string asAnimName)
        {
            AnimRenderPath animPath = GetRenderPath<AnimRenderPath>();
            if (animPath != null)
                animPath.PlayAnimation(afDelay, aeType, asAnimName);
        }

        public void MoveTo(Vector2 destination, float duration, bool smooth)
        {
            MoveTo(destination, duration, smooth, "");
        }

        [ConsoleMethod]
        public void MoveTo(Vector2 destination, float duration, bool smooth, string onCompletionMessage)
        {
            _positionInterval = new Vector2Interval(Position, destination, duration, smooth);
            _positionIntervalMessage = onCompletionMessage;
        }

        public void RotateTo(float endingRotation, float duration, bool smooth)
        {
            RotateTo(endingRotation, duration, smooth, "");
        }

        [ConsoleMethod]
        public void RotateTo(float endingRotation, float duration, bool smooth, string onCompletionMessage)
        {
            _rotationInterval = new FloatInterval(Rotation, endingRotation, duration, smooth);
            _rotationIntervalMessage = onCompletionMessage;
        }

        public void ChangeColorTo(Color endColor, float duration, bool smooth)
        {
            ChangeColorTo(endColor, duration, smooth, "");
        }

        [ConsoleMethod]
        public void ChangeColorTo(Color endColor, float duration, bool smooth, string onCompletionMessage)
        {
            _colorInterval = new ColorInterval(Color, endColor, duration, smooth);
            _colorIntervalMessage = onCompletionMessage;
        }

        public void ChangeSizeTo(Vector2 endSize, float duration, bool smooth)
        {
            ChangeSizeTo(endSize, duration, smooth, "");
        }

        [ConsoleMethod]
        public void ChangeSizeTo(Vector2 endSize, float duration, bool smooth, string onCompletionMessage)
        {
            _sizeInterval = new Vector2Interval(Size, endSize, duration, smooth);
            _sizeIntervalMessage = onCompletionMessage;
        }

        public void ChangeSizeTo(float endSize, float duration, bool smooth)
        {
            ChangeSizeTo(endSize, duration, smooth, "");
        }

        public void ChangeSizeTo(float endSize, float duration, bool smooth, string onCompletionMessage)
        {
            ChangeSizeTo(new Vector2(endSize, endSize), duration, smooth, onCompletionMessage);
        }

        public void CancelTween()
        {
            _positionInterval = null;
            _positionIntervalMessage = null;
            _rotationInterval = null;
            _rotationIntervalMessage = null;
            _colorInterval = null;
            _colorIntervalMessage = null;
            _sizeInterval = null;
            _sizeIntervalMessage = null;
        }

        public override bool IsInside(Camera camera, Vector2 screenPos)
        {
            Vector2 position = camera.WorldToScreen(Position);
            Vector2 size = camera.WorldSizeToScreenSize(Size);

            Matrix rot = Matrix.CreateRotationZ(MathHelper.ToRadians(Rotation));
            Vector2 trasformedSize = Vector2.Transform(size, rot);

            Vector2 corner = position - trasformedSize / 2;
            Vector2 v1 = Vector2.Transform(Vector2.UnitX * size.X, rot);
            Vector2 v2 = Vector2.Transform(Vector2.UnitY * size.Y, rot);
            Vector2 v = screenPos - corner;

            float vdotv1 = Vector2.Dot(v, v1);
            float vdotv2 = Vector2.Dot(v, v2);
            if (vdotv1 >= 0 && vdotv1 <= Vector2.Dot(v1, v1)
                && vdotv2 >= 0 && vdotv2 <= Vector2.Dot(v2, v2))
                return true;

            return false;
        }

        public virtual void LevelUnloaded() { }

        public override void Update(GameTime aTime)
        {
            float dt = (float)aTime.ElapsedGameTime.TotalSeconds;

            _renderPath.Update(aTime);

            if (_positionInterval != null && _positionInterval.ShouldStep)
            {
                Position = _positionInterval.Step(dt);
                if (!_positionInterval.ShouldStep)
                {
                    if (_positionIntervalMessage != "")
                    {
                        Switchboard.Instance.Broadcast(new Message(_positionIntervalMessage, this));
                    }
                }
            }

            if (_rotationInterval != null && _rotationInterval.ShouldStep)
            {
                Rotation = _rotationInterval.Step(dt);
                if (!_rotationInterval.ShouldStep)
                {
                    if (_rotationIntervalMessage != "")
                    {
                        Switchboard.Instance.Broadcast(new Message(_rotationIntervalMessage, this));
                    }
                }
            }

            if (_colorInterval != null && _colorInterval.ShouldStep)
            {
                Color = _colorInterval.Step(dt);
                if (!_colorInterval.ShouldStep)
                {
                    if (_colorIntervalMessage != "")
                    {
                        Switchboard.Instance.Broadcast(new Message(_colorIntervalMessage, this));
                    }
                }
            }

            if (_sizeInterval != null && _sizeInterval.ShouldStep)
            {
                Size = _sizeInterval.Step(dt);
                if (!_sizeInterval.ShouldStep)
                {
                    if (_sizeIntervalMessage != "")
                    {
                        Switchboard.Instance.Broadcast(new Message(_sizeIntervalMessage, this));
                    }
                }
            }
        }

        public override void Render(GameTime aTime, Camera aCamera, GraphicsDevice aDevice, SpriteBatch aBatch)
        {
            _renderPath.Render(aTime, aCamera, aDevice, aBatch);
        }

        public static Actor GetNamed(string asName)
        {
            if(s_NameList.ContainsKey(asName))
                return s_NameList[asName];
            
            return null;
        }

        public static bool IsAnimFile(string asFileName)
        {
            int numberSeparator = asFileName.LastIndexOf("_");
            int numDigits = asFileName.Length - numberSeparator - 1;

            bool bValidNumber = true;
            // So you're saying I've got a chance?
            if (numberSeparator > 0 && numDigits > 0)
            {
                // Now see if all of the digits between _ and . are numbers (i.e. test_001.jpg).
                for (int i = 1; i <= numDigits; ++i)
                {
                    if (!Char.IsDigit(asFileName[numberSeparator + i]))
                    {
                        bValidNumber = false;
                        break;
                    }
                }
            }

            return !(numberSeparator == -1 || numDigits <= 0 || !bValidNumber);
        }

        [ConsoleMethod]
        public static Actor Create()
        {
            return new Actor();
        }
    }
}
