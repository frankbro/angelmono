﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using AngelMono.Util;
using AngelMono.Infrastructure;
using AngelMono.Infrastructure.Console;
using AngelMono.Rendering;
using AngelMono.Editor;

#if WINDOWS
using System.Drawing.Design;
#endif


namespace AngelMono.Actors
{
    public class ParticleActor : Actor
    {
        public struct Particle
        {
            public Vector2 _pos;
            public Vector2 _vel;
            public float _age;
            public float _lifetime;
            public float _rotation;
            public Color _color;
            public float _scale;
        };

        private Particle[] _particles;
	    private int		_maxParticlesAlive = 0;
	    private int		_numParticlesAlive;

	    private float	_particlesPerSecond = 20.0f;
	    private float	_generationResidue = 0.0f;

	    private float	_systemLifetime = 0.0f;	    // How long the system will live.   (seconds) Good for one shot effects.
	    private float	_particleLifetime = 1.0f;	// How long the particles will live. (seconds)

	    private float	_spreadRadians = 3.14f;		// How much the particles can deviate in direction from the system.

	    private Color   _endColor = Color.White;    // We use the Actor _color as the start color.

	    private float	_minSpeed = 2.0f;           // At emission, we choose a random speed between minSpeed and maxSpeed.
	    private float	_maxSpeed = 4.0f;

        private float   _minRotation = 0.0f;
        private float   _maxRotation = 0.0f;

        private float   _startScale = 1.0f;
	    private float   _endScale = 1.0f;    // Respects the starting aspect ratio of the sprite.

	    private Vector2 _gravity = new Vector2(0, 0.0f);

        [ConsoleProperty]
        public float MinRotation
        {
            get { return _minRotation; }
            set { _minRotation = value; }
        }

        [ConsoleProperty]
        public float MaxRotation
        {
            get { return _maxRotation; }
            set { _maxRotation = value; }
        }

        [ConsoleProperty]
        public float ParticlesPerSecond
        {
            get { return _particlesPerSecond; }
            set 
            {
                if (value < 0.0f)
                    value = 0.0f;
                _particlesPerSecond = value; 
            }
        }

        [ConsoleProperty]
        public float SystemLifetime
        {
            get { return _systemLifetime; }
            set 
            {
                if (value < 0.0f)
                    value = 0.0f;
                _systemLifetime = value; 
            }
        }

        [ConsoleProperty]
        public float ParticleLifetime
        {
            get { return _particleLifetime; }
            set 
            {
                if (value < 0.0f)
                    value = 0.0f;
                _particleLifetime = value; 
            }
        }

        [ConsoleProperty]
        public float Spread
        {
            get { return _spreadRadians; }
            set { _spreadRadians = value; }
        }

        [ConsoleProperty]
        public float StartScale
        {
            get { return _startScale; }
            set { _startScale = value; }
        }

        [ConsoleProperty]
        public float EndScale
        {
            get { return _endScale; }
            set { _endScale = value; }
        }

        [ConsoleProperty]
        [Editor(typeof(XnaColorEditor), typeof(UITypeEditor))]
        public Color EndColor
        {
            get { return _endColor; }
            set { _endColor = value; }
        }

        [ConsoleProperty]
        public Vector2 Gravity
        {
            get { return _gravity; }
            set { _gravity = value; }
        }

        [ConsoleProperty]
        public int MaxParticles
        {
            get { return _maxParticlesAlive; }
            set
            {
                if (_maxParticlesAlive == value)
                    return;

                if (value <= 0)
                    value = 1;
                
                _maxParticlesAlive = value;
                _particles = new Particle[value];                

                // Make them all available.   Age < 0.0f = free.
                for (int i = 0; i < _maxParticlesAlive; ++i)
                {
                    _particles[i]._age = -1.0f;
                }
            }
        }

        [ConsoleProperty]
        public float MinSpeed
        {
            get { return _minSpeed; }
            set { _minSpeed = value; }
        }

        [ConsoleProperty]
        public float MaxSpeed
        {
            get { return _maxSpeed; }
            set { _maxSpeed = value; }
        }

        public Particle[] Particles
        {
            get { return _particles; }
        }

        [Editor(typeof(AllOfTypeObjectSelector<TextureRenderPath>), typeof(UITypeEditor))]
        public override IRenderPath RenderPath
        {
            get { return base.RenderPath; }
            set
            {
                TextureRenderPath renderPath = value as TextureRenderPath;
                if (renderPath == null)
                    throw new Exception("Particle actors require that the render path implment ITextureRenderPath.");

                base.RenderPath = new ParticleRenderPath(this, renderPath);
            }
        }

        public ParticleActor()
        {
            Color = Color.Red;
            // This has no effect on the actual particle system, just makes it easier to select
            Size = new Vector2(4, 4);
            MaxParticles = 50;

            TextureRenderPath renderPath = _renderPath as TextureRenderPath;
            if (renderPath == null)
                throw new Exception("Particle actors require that the render path implment ITextureRenderPath.");

            _renderPath = new ParticleRenderPath(this, renderPath);
        }

        public void SetSpeedRange(float minSpeed, float maxSpeed)
        {
            _minSpeed = minSpeed;
            _maxSpeed = maxSpeed;
        }

        public override void Update(GameTime aTime)
        {
            base.Update(aTime);

	        if (_maxParticlesAlive == 0)
		        return;

	        //
	        // Update existing particles.
	        //
	        _numParticlesAlive = 0;
	        for (int i=0; i < _maxParticlesAlive; ++i)
	        {
                if (_particles[i]._age < 0.0f)
			        continue;

                if (_particles[i]._age < _particles[i]._lifetime)
		        {
                    _particles[i]._age += (float)aTime.ElapsedGameTime.TotalSeconds;

                    if (_particles[i]._age < _particles[i]._lifetime)
			        {
				        // Where are we in our lifespan? (0..1)
                        float lifePercent = _particles[i]._age / _particles[i]._lifetime;

				        // Determine current position based on last known position, velocity and
				        // current time delta.
                        _particles[i]._pos = _particles[i]._pos + _particles[i]._vel * (float)aTime.ElapsedGameTime.TotalSeconds;

				        // Update our current velocity, which will be used next update.
                        _particles[i]._vel = _particles[i]._vel + _gravity * (float)aTime.ElapsedGameTime.TotalSeconds;

                        Vector4 startColor = Color.ToVector4();
                        Vector4 endColor = _endColor.ToVector4();
                        Vector4 currentColor = new Vector4();
				        currentColor.X = MathHelper.Lerp(startColor.X, endColor.X, lifePercent);
                        currentColor.Y = MathHelper.Lerp(startColor.Y, endColor.Y, lifePercent);
                        currentColor.Z = MathHelper.Lerp(startColor.Z, endColor.Z, lifePercent);
                        currentColor.W = MathHelper.Lerp(startColor.W, endColor.W, lifePercent);
                        _particles[i]._color = new Color(currentColor);
				        _particles[i]._scale = MathHelper.Lerp(_startScale, _endScale, lifePercent);
        				
				        ++_numParticlesAlive;
			        }
			        else 
			        {
                        _particles[i]._age = -1.0f;
			        }
		        }
	        }

	        // Systems with 0.0f lifetime live forever.
	        if (_systemLifetime > 0.0f)
		        _systemLifetime -= (float)aTime.ElapsedGameTime.TotalSeconds;

	        // We're dead, but we're waiting for our particle to finish.
	        if (_systemLifetime < 0.0f)
	        {
		        if (_numParticlesAlive == 0)
		        {
			        Destroy();
		        }

		        return;
	        }

	        //
	        // Create new particles.
	        //

	        // Add in any residual time from last emission.
            float dt = (float)aTime.ElapsedGameTime.TotalSeconds;
	        dt += _generationResidue;

	        int numParticlesToGenerate = (int)(_particlesPerSecond * dt);
	        _generationResidue = _particlesPerSecond * dt - (float)numParticlesToGenerate;
        	
	        if (numParticlesToGenerate > 0)
	        {		
		        float rot = MathHelper.ToRadians(Rotation);
		        float particleRot;

		        int particlesGenerated = 0;
		        for (int i=0; i<_maxParticlesAlive; ++i)
		        {
			        if (_particles[i]._age < 0.0f)
			        {
                        _particles[i]._age = 0.0f;
                        _particles[i]._lifetime = _particleLifetime;
                        _particles[i]._pos = Position;
                        _particles[i]._color = Color;
                        _particles[i]._scale = _startScale;
        				
				        particleRot = MathUtil.RandomFloatWithError(rot, _spreadRadians);
				        float speed = MathUtil.RandomFloatInRange(_minSpeed, _maxSpeed);
                        _particles[i]._vel = new Vector2(speed * (float)Math.Cos(particleRot), speed * (float)Math.Sin(particleRot));

                        _particles[i]._rotation = MathUtil.RandomFloatInRange(_minRotation, _maxRotation);

				        ++particlesGenerated;

				        // If we've generated enough, break out.
				        if (particlesGenerated == numParticlesToGenerate)
					        break;
			        }
		        }
	        }
        }

        [ConsoleMethod]
        public static new ParticleActor Create()
        {
            return new ParticleActor();
        }
    }
}
