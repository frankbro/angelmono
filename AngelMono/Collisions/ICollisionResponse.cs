﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AngelMono.Physics;

namespace AngelMono.Collisions
{
    public interface ICollisionResponse
    {
        void Execute(PhysicsEventActor struck, PhysicsEventActor striker);
    }
}
