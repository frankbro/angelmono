﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using AngelMono.Actors;
using AngelMono.Infrastructure;
using AngelMono.Infrastructure.Console;
using AngelMono.Infrastructure.Logging;
using AngelMono.Messaging;
using AngelMono.Util;

using FarseerPhysics;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using FarseerPhysics.Factories;
//using Box2D.XNA;

namespace AngelMono.Physics
{
    public delegate void CollisionHandler(PhysicsActor actorA, PhysicsActor actorB, Contact contactPoint);

    public class PhysicsActor : Actor, IPhysicalObject
    {
        public enum ShapeType
        {
            Box,
            Circle
        }

        protected float _fDensity = 1.0f;
        protected float _fFriction = 0.3f;
        protected float _fRestitution = 0.0f;
        protected bool _bIsSensor = false;
        protected int _iGroupIndex = 0;
        protected int _iCollisionFlags = -1;
        protected bool _bFixedRotation = false;

        protected Body _physBody;

        public bool IsInitialized 
        { 
            get { return _physBody != null; } 
        }

        public Body Body
        {
            get { return _physBody; }
            set { _physBody = value; }
        }

        [ConsoleProperty]
        public override Vector2 Size
        {
            get { return base.Size; }
            set
            {
                if (_physBody != null)
                {
                    Log.Instance.Log("WARNING: SetSize() had no effect - don't change this actor after physics have been initialized.");
                    return;
                }

                base.Size = value;
            }
        }

        [ConsoleProperty]
        public override Vector2 Position
        {
            get { return base.Position; }
            set
            {
                if (_physBody != null)
                    _physBody.SetTransform(value, MathHelper.ToRadians(-Rotation));

                base.Position = value;
            }
        }

        [ConsoleProperty]
        public override float Rotation
        {
            get { return base.Rotation; }
            set
            {
                if (_physBody != null)
                    _physBody.SetTransform(Position, MathHelper.ToRadians(-value));

                base.Rotation = value;
            }
        }

        [ConsoleProperty]
        public float Density
        {
            get { return _fDensity; }
            set
            {
                if (_physBody != null)
                {
                    Log.Instance.Log("WARNING: SetDensity had no effect - don't change this actor after physics has been initialized.");
                    return;
                }

                _fDensity = value;
            }
        }

        [ConsoleProperty]
        public float Friction
        {
            get { return _fFriction; }
            set
            {
                if (_physBody != null)
                {
                    Log.Instance.Log("WARNING: SetFriction had no effect - don't change this actor after physics has been initialized.");
                    return;
                }

                _fFriction = value;
            }
        }

        [ConsoleProperty]
        public float Restitution
        {
            get { return _fRestitution; }
            set
            {
                if (_physBody != null)
                {
                    Log.Instance.Log("WARNING: set_Restitution had no effect - don't change this actor after physics has been initialized.");
                    return;
                }

                _fRestitution = value;
            }
        }

        [ConsoleProperty]
        public bool IsSensor
        {
            get { return _bIsSensor; }
            set
            {
                if (_physBody != null)
                {
                    Log.Instance.Log("WARNING: set_IsSensor had no effect - don't change this actor after physics has been initialized.");
                    return;
                }

                _bIsSensor = value;
            }
        }

        [ConsoleProperty]
        public int GroupIndex
        {
            get { return _iGroupIndex; }
            set
            {
                if (_physBody != null)
                {
                    Log.Instance.Log("WARNING: set_GroupIndex had no effect - don't change this actor after physics has been initialized.");
                    return;
                }

                _iGroupIndex = value;
            }
        }

        [ConsoleProperty]
        public int CollisionFlags
        {
            get { return _iCollisionFlags; }
            set
            {
                if (_physBody != null)
                {
                    Log.Instance.Log("WARNING: set_CollisionFlags had no effect - don't change this actor after physics has been initialized.");
                    return;
                }

                _iCollisionFlags = value;
            }
        }

        [ConsoleProperty]
        public bool FixedRotation
        {
            get { return _bFixedRotation; }
            set
            {
                if (_physBody != null)
                {
                    Log.Instance.Log("WARNING: set_FixedRotation had no effect - don't change this actor after physics has been initialized.");
                    return;
                }

                _bFixedRotation = value;
            }
        }

        [ConsoleProperty]
        public bool AutoInitPhysics { get; set; }

        public event CollisionHandler Collision;

        public PhysicsActor()
            : base()
        {
            AutoInitPhysics = true;
        }

        public override void AddedToWorld()
        {
            base.AddedToWorld();

            if (AutoInitPhysics)
                InitPhysics();
        }

        public virtual void InitPhysics()
        {
            if (_physBody != null)
            {
                Log.Instance.Log("WARNING: Call to InitPhysics had no effect.  Actor has already had physics initialized.");
                return;
            }

            ShapeType shapeType = ShapeType.Box;
            //Box2D.XNA.World physicsWorld = AngelMono.Infrastructure.World.Instance.PhysicsWorld;
			FarseerPhysics.Dynamics.World physicsWorld = AngelMono.Infrastructure.World.Instance.PhysicsWorld;

            Shape shape;
            
            switch (shapeType)
            {
                case ShapeType.Box:
                    PolygonShape box = new PolygonShape(0);
                    box.SetAsBox(0.5f * Size.X, 0.5f * Size.Y);
                    shape = box;
                    break;
                case ShapeType.Circle:
                    CircleShape circle = new CircleShape(0.5f * Size.X, 0);
                    shape = circle;
                    break;
                default:
                    Log.Instance.Log("InitPhysics(): Invalid shape type given");
                    return;
            }
			
            /*FixtureDef fixtureDef = new FixtureDef();
            fixtureDef.density = _fDensity; // TODO in shape?
            fixtureDef.friction = _fFriction;
            fixtureDef.restitution = _fRestitution;
            fixtureDef.isSensor = _bIsSensor;
            fixtureDef.shape = shape;*/
			
            /*Fixture fixture = FixtureFactory();
			shape.Density = _fDensity;
			fixture.Friction = _fDensity;
			fixture.Restitution = _fRestitution;
			fixture.IsSensor = _bIsSensor;
			fixture.Shape = shape;*/
			
            if (_iCollisionFlags != -1)
            {
                //shape->maskBits = (short)collisionFlags;
                //shape->categoryBits = (short)collisionFlags;
            }

            /*BodyDef bodyDef = new BodyDef();
            bodyDef.type = _fDensity == 0.0f ? BodyType.Static : BodyType.Dynamic;
            bodyDef.active = true;
            bodyDef.userData = this;
            bodyDef.position = this.Position;
            bodyDef.angle = MathHelper.ToRadians(-Rotation);
            bodyDef.fixedRotation = _bFixedRotation;*/
            Body body = BodyFactory.CreateBody(physicsWorld);
			body.BodyType = _fDensity == 0.0f ? BodyType.Static : BodyType.Dynamic;
			body.Enabled = true;
			body.UserData = this;
			body.Position = this.Position;
			body.Rotation = this.Rotation;
			body.FixedRotation = _bFixedRotation;
			
			shape.Density = _fDensity;
			Fixture fixture = body.CreateFixture(shape);
			fixture.Friction = _fDensity;
			fixture.Restitution = _fRestitution;
			fixture.IsSensor = _bIsSensor;

            /*_physBody = physicsWorld.CreateBody(bodyDef);
            _physBody.CreateFixture(fixtureDef);*/

            CustomInitPhysics();
        }

        public void InitPhysics(float density, float friction, float restitution)
        {
            if (_physBody != null)
            {
                Log.Instance.Log("WARNING: Call to InitPhysics had no effect.  Actor has already had physics initialized.");
                return;
            }

            _fDensity = density;
            _fFriction = friction;
            _fRestitution = restitution;
            InitPhysics();
        }

        public void InitPhysics(float density, float friction, float restitution, ShapeType shapeType)
        {
            if (_physBody != null)
            {
                Log.Instance.Log("WARNING: Call to InitPhysics had no effect.  Actor has already had physics initialized.");
                return;
            }

            _fDensity = density;
            _fFriction = friction;
            _fRestitution = restitution;
            InitPhysics();
        }

        public void InitPhysics(float density, float friction, float restitution,
            ShapeType shapeType, bool isSensor, int groupIndex, int collisionFlags, bool fixedRotation)
        {
            if (_physBody != null)
            {
                Log.Instance.Log("WARNING: Call to InitPhysics had no effect.  Actor has already had physics initialized.");
                return;
            }

            _fDensity = density;
            _fFriction = friction;
            _fRestitution = restitution;
            _bIsSensor = isSensor;
            _iGroupIndex = groupIndex;
            _iCollisionFlags = collisionFlags;
            _bFixedRotation = fixedRotation;
            InitPhysics();
        }

        public virtual void CustomInitPhysics() { }
        public virtual bool HandlesCollisionEvents() { return false; }

        public void DestroyPhysics()
        {
            CustomDestroyPhysics();

            if (_physBody != null)
            {
                //_physBody.SetUserData(null);
				_physBody.UserData = null;
                //Box2D.XNA.World world = AngelMono.Infrastructure.World.Instance.PhysicsWorld;
				FarseerPhysics.Dynamics.World world = AngelMono.Infrastructure.World.Instance.PhysicsWorld;
                //world.DestroyBody(_physBody);
				world.RemoveBody(_physBody);

                _physBody = null;
            }
        }

        public virtual void CustomDestroyPhysics() { }

        public void ApplyForce(Vector2 force, Vector2 point)
        {
            if (_physBody != null)
                _physBody.ApplyForce(force, point + Position);
        }

        // apply a local space force on the object
        public void ApplyLocalForce(Vector2 force, Vector2 point)
        {
            if (_physBody != null)
                _physBody.ApplyForce(_physBody.GetWorldVector(force), point + Position);
        }

        public void ApplyTorque(float torque)
        {
            if (_physBody != null)
                _physBody.ApplyTorque(torque);
        }

        public void ApplyImpulse(Vector2 impulse, Vector2 point)
        {
            if (_physBody != null)
                _physBody.ApplyLinearImpulse(impulse, point);
        }

        public virtual void SyncPosRot()
        {
            base.Position = _physBody.Position;
            base.Rotation = -MathHelper.ToDegrees(_physBody.Rotation);
        }

        public override void RemovedFromWorld()
        {
            DestroyPhysics();

            base.RemovedFromWorld();
        }

        internal virtual void OnCollision(PhysicsActor otherActor, Contact point)
        {
            if (Collision != null)
                Collision(this, otherActor, point);
        }

        [ConsoleMethod]
        public static new PhysicsActor Create()
        {
            return new PhysicsActor();
        }

        // Use this if you want to allow a super class to force a specefic render
        // position.  
        protected void ForcePosRot(Vector2 position, float frotation)
        {
            base.Position = position;
            base.Rotation = frotation;
        }
    }
}
