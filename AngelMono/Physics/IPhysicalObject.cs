﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AngelMono.Physics
{
    /// <summary>
    /// This interface is for objects that need to have phsical
    /// initialization / deinitialization when being forcibly moved
    /// by things like the editor.
    /// </summary>
    public interface IPhysicalObject
    {
        bool IsInitialized { get; }

        void DestroyPhysics();
        void InitPhysics();

    }
}
