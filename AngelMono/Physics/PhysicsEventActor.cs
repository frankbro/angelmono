﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using AngelMono.Actors;
using AngelMono.Infrastructure.Console;
using AngelMono.Collisions;
//using Box2D.XNA;
using FarseerPhysics;
using FarseerPhysics.Dynamics.Contacts;

namespace AngelMono.Physics
{
    public class PhysicsEventActor : PhysicsActor
    {
        private string _collisionId;
        private List<PhysicsEventActor> _collisions = new List<PhysicsEventActor>();
        private Dictionary<string, List<ICollisionResponse>> _collisionResponseTable = new Dictionary<string, List<ICollisionResponse>>();

        [ConsoleProperty]
        public string CollisionId
        {
            get { return _collisionId; }
            set { _collisionId = value.ToUpper(); }
        }

        public PhysicsEventActor()
        {

        }

        public override void Update(GameTime aTime)
        {
            ProcessCollisions();
            base.Update(aTime);
        }

        [ConsoleMethod]
        public virtual void RegisterCollisionResponse(string key, ICollisionResponse colResponse)
        {
            key = key.ToUpper();
            if(!_collisionResponseTable.ContainsKey(key))
                _collisionResponseTable.Add(key, new List<ICollisionResponse>());
            

            _collisionResponseTable[key].Add(colResponse);
        }

        public virtual void RemoveCollisionResponse(string key, ICollisionResponse colResponse)
        {
            if(_collisionResponseTable.ContainsKey(key))
            {
                _collisionResponseTable[key].Remove(colResponse);
                if(_collisionResponseTable[key].Count == 0)
                    _collisionResponseTable.Remove(key);
            }
        }

        public override string ToString()
        {
            return String.Format("PhysicsEventActor<{0}>", Name);
        }

        public virtual void OnNamedEvent(string eventId) { }

        internal override void OnCollision(PhysicsActor otherActor, Contact point)
        {
            if (otherActor is PhysicsEventActor)
            {
                AddCollision((PhysicsEventActor)otherActor);
            }

            base.OnCollision(otherActor, point);
        }

        protected void AddCollision(PhysicsEventActor otherActor)
        {
            _collisions.Add(otherActor);
        }

        protected virtual void ProcessCollisions()
        {
            ProcessCollisionsInternal();
            _collisions.Clear();
        }

        protected virtual void ProcessCollisionsInternal()
        {
            for (int i = 0; i < _collisions.Count; ++i)
            {
                PhysicsEventActor striker = _collisions[i];
                if (striker._collisionId != null &&
                    _collisionResponseTable.ContainsKey(striker._collisionId))
                {
                    foreach (ICollisionResponse response in _collisionResponseTable[striker._collisionId])
                    {
                        response.Execute(this, striker);
                    }
                }
            }
        }

        [ConsoleMethod]
        public static new PhysicsEventActor Create()
        {
            return new PhysicsEventActor();
        }
    }
}
