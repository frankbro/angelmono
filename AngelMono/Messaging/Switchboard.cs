﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Microsoft.Xna.Framework;
using AngelMono.Infrastructure.Console;
using AngelMono.Infrastructure;
using AngelMono.Infrastructure.Logging;

namespace AngelMono.Messaging
{
    public delegate void MessageHandler(Message message);

    public class Switchboard
    {
        #region Singleton implementation
        private static Switchboard s_Instance = new Switchboard();

        public static Switchboard Instance
        {
            get { return s_Instance; }
        }
        #endregion

        private class MessageTimer
        {
            public Message _message;
            public float _timeRemaining;

            public MessageTimer(Message message, float timeRemaining)
            {
                _message = message;
                _timeRemaining = Math.Max(0.0f, timeRemaining);
            }

            public void Tick(float dt)
            {
                _timeRemaining -= dt;
            }
        }

        private Dictionary<string, MessageHandler> _subscriptions = new Dictionary<string,MessageHandler>();
        private Queue<Message> _messages = new Queue<Message>();
        private LinkedList<MessageTimer> _delayedMessages = new LinkedList<MessageTimer>();

        public Switchboard()
        {
            ConsoleVariable var = DeveloperConsole.Instance.ItemManager.GetCVar("theSwitchboard");
            var.Value = this;
        }

        public void Broadcast(Message message)
        {
            if (World.Instance.IsSimulating)
                _messages.Enqueue(message);
            else
                Log.Instance.Log("[DEBUG] Ignoring message sent while world is not simulating: " + message.ToString());
        }

        [ConsoleMethod]
        public void Broadcast(string message)
        {
            Broadcast(new Message(message));
        }

        public void DeferredBroadcast(Message message, float delay)
        {
	        _delayedMessages.AddLast(new MessageTimer(message, delay));
        }

        public void Update(GameTime gameTime)
        {
            float dt = (float)gameTime.ElapsedGameTime.TotalSeconds;
            for (LinkedListNode<MessageTimer> node = _delayedMessages.First; node != null;)
            {
                node.Value.Tick(dt);
                if (node.Value._timeRemaining <= 0.0f)
                {
                    Broadcast(node.Value._message);
                    LinkedListNode<MessageTimer> nextNode = node.Next;
                    _delayedMessages.Remove(node);
                    node = nextNode;
                }
                else
                    node = node.Next;
            }
        }

        public void SendAllMessages()
        {
            while (_messages.Count > 0)
	        {
                Message nextMessage = _messages.Dequeue();
                if(_subscriptions.ContainsKey(nextMessage.MessageName))
                {
                    MessageHandler handler = _subscriptions[nextMessage.MessageName];
                    if(handler != null)
                        handler(nextMessage);
                }
	        }
        }

        public MessageHandler this[string asMessage]
        {
            get
            {
                if (_subscriptions.ContainsKey(asMessage))
                    return _subscriptions[asMessage];
                return null;
            }
            set
            {
                if (_subscriptions.ContainsKey(asMessage))
                {
                    if (value == null)
                    {
                        _subscriptions.Remove(asMessage);
                    }
                    else
                    {
                        _subscriptions[asMessage] = value;
                    }
                }
                else if (value != null)
                {
                    _subscriptions.Add(asMessage, value);
                }
            }
        }
    }
}
