﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using AngelMono.Infrastructure.Console;
using System.IO;

namespace AngelMono.Rendering
{
    [ConsoleEnum]
    public enum TiledAnimDirection
    {
        Horizontal,
        Vertical
    }

    public class TileInfo
    {
        private string _sFromFile;

        public string FromFile
        {
            get { return _sFromFile; }
        }

        [ConsoleProperty]
        public bool IsAnim { get; set; }

        [ConsoleProperty]
        public TiledAnimDirection AnimDirection { get; set; }

        [ConsoleProperty]
        public Vector2 BeginFrame { get; set; }

        [ConsoleProperty]
        public Vector2 EndFrame { get; set; }

        [ConsoleProperty]
        public Vector2 FrameSize { get; set; }
        
        [ConsoleProperty]
        public Vector2 InitialSkip { get; set; }

        [ConsoleProperty]
        public Vector2 Border { get; set; }

        public TileInfo(string forFile)
        {
            _sFromFile = forFile;
            IsAnim = false;
        }
    }

    public class TileInfoCache
    {
        private static TileInfoCache s_Instance = new TileInfoCache();

        public static TileInfoCache Instance
        {
            get { return s_Instance; }
        }


        private Dictionary<string, TileInfo> _InfoCache = new Dictionary<string,TileInfo>();

        private TileInfoCache()
        {

        }

        public bool IsCached(string file)
        {
            return _InfoCache.ContainsKey(file);
        }

        public TileInfo GetForFile(string file)
        {
            if(_InfoCache.ContainsKey(file))
                return _InfoCache[file];

            if (!HasTileInfo(file))
                return null;

            TileInfo newInfo = new TileInfo(file);
            DeveloperConsole.Instance.Using(newInfo);
            DeveloperConsole.Instance.ExecuteFile(Path.Combine("Content", file + ".tileinfo"));
            DeveloperConsole.Instance.EndUsing();

            return newInfo;
        }

        public static bool HasTileInfo(string asFileName)
        {
            return File.Exists(Path.Combine("Content", asFileName + ".tileinfo"));
        }
    }
}
