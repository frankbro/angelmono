﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using AngelMono.Infrastructure.Logging;
using AngelMono.Actors;
using Microsoft.Xna.Framework;
using AngelMono.Infrastructure;
using AngelMono.Infrastructure.Console;

namespace AngelMono.Rendering
{
    public class MultiAnimRenderPath : AnimRenderPath
    {
        private Actor _target;
        private Dictionary<string, AnimRenderPath> _renderPaths = new Dictionary<string,AnimRenderPath>();
        private AnimRenderPath _currentAnim;
        private string _currentAnimName;

        public override Texture2D CurrentTexture
        {
            get 
            {
                if (_currentAnim != null)
                    return _currentAnim.CurrentTexture;
                return null;
            }
        }

        public MultiAnimRenderPath(Actor target)
        {
            _target = target;
        }

        public MultiAnimRenderPath(Actor target, string animDef)
        {
            _target = target;
            LoadAnimDefinition(animDef);
        }

        public override void PlayAnimation()
        {
            if (_currentAnim != null)
                _currentAnim.PlayAnimation();
        }

        public override void PlayAnimation(string asName)
        {
            if (_currentAnimName == asName)
                return;

            if (_renderPaths.ContainsKey(asName))
            {
                _currentAnimName = asName;
                _currentAnim = _renderPaths[asName];
                _currentAnim.PlayAnimation();
            }
            else
                Log.Instance.Log("[MultiAnimRenderPath]: Could not find animation {0} for actor {1}", asName, _target.Name);
        }

        public override void PlayAnimation(float afDelay, AnimationType aeType, string asMessage)
        {
            if(_currentAnim != null)
                _currentAnim.PlayAnimation(afDelay, aeType, asMessage);
        }


        public override void Update(GameTime aTime)
        {
            if (_currentAnim != null)
                _currentAnim.Update(aTime);
        }

        public override void Render(GameTime aTime, Camera aCamera, GraphicsDevice aDevice, SpriteBatch aBatch)
        {
            if (_currentAnim != null)
            {
                _currentAnim.Effects = Effects;
                _currentAnim.Render(aTime, aCamera, aDevice, aBatch);
            }
        }

        private void LoadAnimDefinition(string animDef)
        {
            string currentAnimName = null;
            AnimRenderPath currentAnim = null;

            // For the duration of this call, "Begin/EndAnimation" will report the animation back to this
            // function.  This is a a weird hack of the console system that breaks thread safety.
            DeveloperConsole.Instance.ItemManager.AddCommand("BeginAnimation", x => {
                DeveloperConsole.VerifyArgs(x, typeof(string), typeof(AnimRenderPath));
                if(currentAnimName != null || currentAnim != null)
                    throw new InvalidOperationException("Calling BeginAnimation() before calling EndAnimation is bad!");
                currentAnimName = (string)x[0];
                currentAnim = (AnimRenderPath)x[1];
                DeveloperConsole.Instance.Using(currentAnim);
                return null;
            });

            DeveloperConsole.Instance.ItemManager.AddCommand("EndAnimation", x => {
                if(currentAnimName == null || currentAnim == null)
                    throw new InvalidOperationException("EndAnimation() without matching BeginAnimation()");
                DeveloperConsole.Instance.EndUsing();
                currentAnim.Target = _target;
                _renderPaths.Add(currentAnimName, currentAnim);
                currentAnimName = null;
                currentAnim = null;
                return null;
            });

            DeveloperConsole.Instance.ExecuteFile("Config\\Animations\\" + animDef + ".anims");

            DeveloperConsole.Instance.ItemManager.RemoveCommand("BeginAnimation");
            DeveloperConsole.Instance.ItemManager.RemoveCommand("EndAnimation");
        }
    }
}
