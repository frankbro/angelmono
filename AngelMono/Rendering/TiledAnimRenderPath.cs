﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AngelMono.Infrastructure.Console;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using AngelMono.Infrastructure;
using AngelMono.Actors;
using System.ComponentModel;
using AngelMono.Editor;

namespace AngelMono.Rendering
{
    

    public class TiledAnimRenderPath : AnimRenderPath
    {
        private string _sFileName;
        private Texture2D _TileMap;
        private Vector2 _FrameSize;
        private Vector2 _BeginFrame;
        private Vector2 _EndFrame;
        private Vector2 _MaxFrames;

        private float _AnimDelay;
        private float _CurrentFrameDelay;
        private int _AnimDirection;
        private AnimationType _AnimType;
        private Vector2 _CurrentFrame;

        [ConsoleProperty]
        public float AnimDelay
        {
            get { return _AnimDelay; }
            set
            {
                _AnimDelay = value;
                Refresh(false);
            }
        }

        [ConsoleProperty]
        public AnimationType AnimType
        {
            get { return _AnimType; }
            set
            {
                _AnimType = value;
                Refresh(false);
            }
        }

        [ConsoleProperty]
        [TypeConverter(typeof(ContentConverter))]
        public string File
        {
            get { return _sFileName; }
            set
            {
                _sFileName = value;
                Refresh(true);
            }
        }

        [ConsoleProperty]
        public Vector2 FrameSize
        {
            get { return _FrameSize; }
            set
            {
                _FrameSize = value;
                Refresh(false);
            }
        }

        [ConsoleProperty]
        public Vector2 BeginFrame
        {
            get { return _BeginFrame; }
            set
            {
                _BeginFrame.X = MathHelper.Clamp(value.X, 0.0f, _MaxFrames.X);
                _BeginFrame.Y = MathHelper.Clamp(value.Y, 0.0f, _MaxFrames.Y);
            }
        }

        [ConsoleProperty]
        public Vector2 EndFrame
        {
            get { return _EndFrame; }
            set
            {
                _EndFrame.X = MathHelper.Clamp(value.X, 0.0f, _MaxFrames.X);
                _EndFrame.Y = MathHelper.Clamp(value.Y, 0.0f, _MaxFrames.Y);
            }
        }

        [ConsoleProperty]
        public TiledAnimDirection AnimDirection
        {
            get;
            set;
        }

        public override Texture2D CurrentTexture
        {
            get { return _TileMap; }
        }

        public override Rectangle? SourceRect
        {
            get
            {
                return new Rectangle((int)(_CurrentFrame.X * _FrameSize.X),
                    (int)(_CurrentFrame.Y * _FrameSize.Y), (int)_FrameSize.X, (int)_FrameSize.Y);
            }
        }

        public override Vector2 Origin
        {
            get
            {
                return new Vector2(_FrameSize.X / 2, _FrameSize.Y / 2);
            }
        }

        public TiledAnimRenderPath()
        {
            _TileMap = ContentHelpers.LoadTexture("white");
        }

        public TiledAnimRenderPath(Actor target)
        {
            Target = target;
            _TileMap = ContentHelpers.LoadTexture("white");
        }

        public TiledAnimRenderPath(Actor target, string asFileName)
        {
            Target = target;
            _sFileName = asFileName;
            Refresh(true);
        }

        public void Refresh(bool reloadTexture)
        {
            if (reloadTexture)
            {
                _TileMap = World.Instance.Game.Content.Load<Texture2D>(_sFileName);

                if (TileInfoCache.HasTileInfo(_sFileName))
                {
                    TileInfo info = TileInfoCache.Instance.GetForFile(_sFileName);
                    FrameSize = info.FrameSize;
                    if (info.IsAnim)
                    {
                        AnimDirection = info.AnimDirection;
                        BeginFrame = info.BeginFrame;
                        EndFrame = info.EndFrame;
                    }
                }
            }

            if (FrameSize != Vector2.Zero)
                _MaxFrames = new Vector2(_TileMap.Width / FrameSize.X, _TileMap.Height / FrameSize.Y);
            else
                _MaxFrames = Vector2.Zero;

            _BeginFrame.X = MathHelper.Clamp(_BeginFrame.X, 0.0f, _MaxFrames.X);
            _BeginFrame.Y = MathHelper.Clamp(_BeginFrame.Y, 0.0f, _MaxFrames.Y);
            _EndFrame.X = MathHelper.Clamp(_EndFrame.X, 0.0f, _EndFrame.X);
            _EndFrame.Y = MathHelper.Clamp(_EndFrame.Y, 0.0f, _EndFrame.Y);
        }

        public override void PlayAnimation()
        {
            _CurrentFrameDelay = _AnimDelay;
            _CurrentFrame = _BeginFrame;
        }

        public override void PlayAnimation(float afDelay, AnimationType aeType, string asName)
        {
            _AnimDelay = afDelay;
            _AnimType = aeType;
        }

        public override void Update(GameTime aTime)
        {
            float dt = (float)aTime.ElapsedGameTime.TotalSeconds;

            if (_AnimDelay > 0.0f)
            {
                _CurrentFrameDelay -= dt;

                while (_CurrentFrameDelay < 0.0f)
                {
                    switch (_AnimType)
                    {
                        case AnimationType.Loop:
                            if (_CurrentFrame == EndFrame)
                                _CurrentFrame = BeginFrame;
                            else
                                IncrementCurrentFrame();
                            break;
                        case AnimationType.PingPong:
                            if (_AnimDirection == 1)
                            {
                                if (_CurrentFrame == EndFrame)
                                {
                                    _AnimDirection = -1;
                                    _CurrentFrame = EndFrame;
                                    DecrementCurrentFrame();
                                }
                                else
                                    IncrementCurrentFrame();

                            }
                            else
                            {
                                if (_CurrentFrame == BeginFrame)
                                {
                                    _AnimDirection = 1;
                                    _CurrentFrame = BeginFrame;
                                    IncrementCurrentFrame();
                                }
                                else
                                    DecrementCurrentFrame();
                            }
                            break;
                        case AnimationType.OneShot:
                            // If we're done with our one shot and they set an animName, let them know it's done.
                            if (_CurrentFrame == EndFrame)
                            {
                                //if (_currentAnimName != null && _currentAnimName.Length > 0)
                                //{
                                //    AnimCallback(_currentAnimName);
                                //}
                            }
                            else
                                IncrementCurrentFrame();
                            break;
                    }

                    _CurrentFrameDelay += _AnimDelay;
                }
            }
        }

        private void DecrementCurrentFrame()
        {
            switch (AnimDirection)
            {
                case TiledAnimDirection.Horizontal:
                    _CurrentFrame.X--;
                    if (_CurrentFrame.X <= 0)
                    {
                        _CurrentFrame.X = _EndFrame.X;
                        _CurrentFrame.Y--;
                    }
                    break;
                case TiledAnimDirection.Vertical:
                    _CurrentFrame.Y--;
                    if (_CurrentFrame.Y <= 0)
                    {
                        _CurrentFrame.Y = _EndFrame.Y;
                        _CurrentFrame.X--;
                    }
                    break;
            }
        }

        private void IncrementCurrentFrame()
        {
            switch (AnimDirection)
            {
                case TiledAnimDirection.Horizontal:
                    _CurrentFrame.X++;
                    if (_CurrentFrame.X >= _MaxFrames.X)
                    {
                        _CurrentFrame.X = _BeginFrame.X;
                        _CurrentFrame.Y++;
                    }
                    break;
                case TiledAnimDirection.Vertical:
                    _CurrentFrame.Y++;
                    if (_CurrentFrame.Y >= _MaxFrames.Y)
                    {
                        _CurrentFrame.Y = _BeginFrame.Y;
                        _CurrentFrame.X++;
                    }
                    break;
            }
        }

        public override void Render(GameTime aTime, Camera aCamera, GraphicsDevice aDevice, SpriteBatch aBatch)
        {
            Vector2 screenPos = aCamera.WorldToScreen(Target.Position);
            Vector2 screenSize = aCamera.WorldSizeToScreenSize(Target.Size);
            Rectangle destRect = new Rectangle((int)screenPos.X, (int)screenPos.Y, (int)screenSize.X, (int)screenSize.Y);

            aBatch.Begin();
            aBatch.Draw(_TileMap, destRect, SourceRect, Target.Color, MathHelper.ToRadians(Target.Rotation),
               Origin, Effects, 0.0f);
            aBatch.End();
        }

        [ConsoleMethod]
        public static TiledAnimRenderPath Create()
        {
            return new TiledAnimRenderPath();
        }
    }
}
