﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AngelMono.Infrastructure.Console;
using AngelMono.Util;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using AngelMono.Infrastructure;
using AngelMono.Infrastructure.Logging;
using AngelMono.Actors;

namespace AngelMono.Rendering
{
    public class AnimSpriteRenderPath : AnimRenderPath
    {
        protected int _startFrame;
        protected int _endFrame;

        protected int _spriteNumFrames = 0;
        protected string _sSpriteFile;
        protected List<Texture2D> _spriteTextures = new List<Texture2D>();
        protected AnimationType _spriteAnimType;
        protected float _spriteAnimDelay;
        protected float _spriteCurrentFrameDelay;
        protected int _spriteAnimDirection;
        protected int _spriteCurrentFrame = 0;

        public bool TileSprite { get; set; }

        [ConsoleProperty]
        public AnimationType SpriteAnimType
        {
            get { return _spriteAnimType; }
            set
            {
                PlayAnimation(_spriteAnimDelay, value);
            }
        }

        [ConsoleProperty]
        public float SpriteAnimSpeed
        {
            get { return _spriteAnimDelay; }
            set
            {
                PlayAnimation(value, _spriteAnimType);
            }
        }

        [ConsoleProperty]
        public int StartFrame 
        {
            get { return _startFrame; }
            set { _startFrame = (int)MathHelper.Clamp(value, 0, _spriteNumFrames - 1); }
        }

        [ConsoleProperty]
        public int EndFrame 
        {
            get { return _endFrame; }
            set { _endFrame = (int)MathHelper.Clamp(value, 0, _spriteNumFrames - 1); }
        }

        public override Texture2D CurrentTexture
        {
            get { return _spriteTextures[_spriteCurrentFrame]; }
        }

        public string Sprite
        {
            get { return _sSpriteFile; }
            set { LoadSpriteFrames(value); }
        }

        public AnimSpriteRenderPath(Actor target, string spriteFile)
        {
            Target = target;
            LoadSpriteFrames(spriteFile);
        }

        private void ClearAnimation()
        {
            _sSpriteFile = null;
            _spriteAnimType = AnimationType.None;
            _spriteAnimDelay = 0.0f;

            _spriteCurrentFrame = 0;

            // Always make sure there's one good texture
            _spriteTextures.Add(ContentHelpers.LoadTexture("white"));
        }

        /// <summary>
        /// We expect the name of the first image to end in _###. 
        /// The number of digits doesn't matter, but internally, we are limited 
        /// to 64 frames.  To change that limit, just change c_MaxSpriteFrames
        /// </summary>
        [ConsoleMethod]
        public void LoadSpriteFrames(string asFirstFileName)
        {
            ClearAnimation();

            _sSpriteFile = asFirstFileName;

            if (!IsAnimFile(asFirstFileName))
            {
                Log.Instance.Log("LoadSpriteFrames() - Bad Format - Expecting somename_###.ext");
                Log.Instance.Log("Attempting to load single texture: " + asFirstFileName);

                Texture2D staticTexture = ContentHelpers.LoadTexture(asFirstFileName);
                if(staticTexture != null)
                    _spriteTextures[0] = staticTexture;
                return;
            }

            _spriteTextures.Clear();    // Remove the 1 good texture we had.

            // reset the number of sprite frames
            _spriteNumFrames = 0;

            int numberSeparator = asFirstFileName.LastIndexOf("_");
            int numDigits = asFirstFileName.Length - numberSeparator - 1;

            // If we got this far, the filename format is correct.
            // The number string is just the digits between the '_' and the file extension (i.e. 001).
            string numberString = asFirstFileName.Substring(numberSeparator + 1, numDigits);

            // Get our starting numberical value.
            int number = int.Parse(numberString);

            // The base name is everything up to the '_' before the number (i.e. somefile_).
            string baseFilename = asFirstFileName.Substring(0, numberSeparator + 1);

            // Keep loading until we stop finding images in the sequence.
            while (true)
            {
                // Build up the filename of the current image in the sequence.
                string newFilename = baseFilename + numberString;

                if (!ContentHelpers.TextureExists(newFilename))
                    break;

                Texture2D nextSprite = ContentHelpers.LoadTexture(newFilename);
                if (nextSprite == null)
                    break;
                _spriteTextures.Add(nextSprite);

                // Bump the number to the next value in the sequence.
                ++number;

                // Serialize the numerical value to it so we can retrieve the string equivalent.
                string newNumberString = number.ToString();

                int numLeadingZeros = 0;
                if (newNumberString.Length < numDigits)
                {
                    numLeadingZeros = numDigits - newNumberString.Length;
                }

                // Do the leading zero padding.
                for (int i = 0; i < numLeadingZeros; ++i)
                {
                    newNumberString = '0' + newNumberString;
                }

                // Save off the newly formulated number string for the next image in the sequence.
                numberString = newNumberString;
            }

            _spriteNumFrames = _spriteTextures.Count;
            StartFrame = 0;
            EndFrame = _spriteNumFrames - 1;
        }
   
        public override void PlayAnimation()
        {
            _spriteAnimDirection = 1;
            _spriteCurrentFrame = StartFrame;
        }

        public override void PlayAnimation(float afDelay, AnimationType aeType, string asAnimName)
        {
            _spriteAnimDirection = 1;

            _spriteCurrentFrameDelay = _spriteAnimDelay = afDelay;
            _spriteAnimType = aeType;
            _spriteCurrentFrame = StartFrame;

            //if (asAnimName != null)
            //    _currentAnimName = asAnimName;
        }

        public override void Update(GameTime aTime)
        {
            float dt = (float)aTime.ElapsedGameTime.TotalSeconds;

            if (_spriteAnimDelay > 0.0f)
            {
                _spriteCurrentFrameDelay -= dt;

                if (_spriteCurrentFrameDelay < 0.0f)
                {
                    while (_spriteCurrentFrameDelay < 0.0f)
                    {
                        switch (_spriteAnimType)
                        {
                            case AnimationType.Loop:
                                if (_spriteCurrentFrame == EndFrame)
                                    _spriteCurrentFrame = StartFrame;
                                else
                                    ++_spriteCurrentFrame;
                                break;
                            case AnimationType.PingPong:
                                if (_spriteAnimDirection == 1)
                                {
                                    if (_spriteCurrentFrame == EndFrame)
                                    {
                                        _spriteAnimDirection = -1;
                                        _spriteCurrentFrame = EndFrame - 1;
                                    }
                                    else
                                        ++_spriteCurrentFrame;

                                }
                                else
                                {
                                    if (_spriteCurrentFrame == StartFrame)
                                    {
                                        _spriteAnimDirection = 1;
                                        _spriteCurrentFrame = StartFrame + 1;
                                    }
                                    else
                                    {
                                        --_spriteCurrentFrame;
                                    }
                                }
                                break;
                            case AnimationType.OneShot:
                                // If we're done with our one shot and they set an animName, let them know it's done.
                                if (_spriteCurrentFrame == EndFrame)
                                {
                                    // Needs to get called before callback, in case they start a new animation.
                                    _spriteAnimType = AnimationType.None;

                                    //if (_currentAnimName != null && _currentAnimName.Length > 0)
                                    //{
                                    //    AnimCallback(_currentAnimName);
                                    //}
                                }
                                else
                                {
                                    _spriteCurrentFrame += _spriteAnimDirection;
                                }
                                break;
                        }

                        _spriteCurrentFrameDelay += _spriteAnimDelay;
                    }
                }
            }
        }

        public override void Render(GameTime aTime, Camera aCamera, GraphicsDevice aDevice, SpriteBatch aBatch)
        {
            Texture2D myTexture = _spriteTextures[_spriteCurrentFrame];
            
            Vector2 screenPos = aCamera.WorldToScreen(Target.Position);
            Vector2 screenSize = aCamera.WorldSizeToScreenSize(Target.Size);
            Rectangle destRect = new Rectangle((int)screenPos.X, (int)screenPos.Y, (int)screenSize.X, (int)screenSize.Y);

            Rectangle? sourceRect = null;
            Vector2 origin = new Vector2(myTexture.Width / 2, myTexture.Height / 2);
            if (TileSprite)
            {
                float ftilesX = screenSize.X / myTexture.Width;
                float ftilesY = screenSize.Y / myTexture.Height;
                sourceRect = new Rectangle(0, 0, (int)(myTexture.Width * ftilesX), (int)(myTexture.Height * ftilesY));
                origin = new Vector2(sourceRect.Value.Width / 2, sourceRect.Value.Height / 2);
            }

            if (TileSprite)
            {
                aBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
                aDevice.SamplerStates[0].AddressU = TextureAddressMode.Wrap;
                aDevice.SamplerStates[0].AddressV = TextureAddressMode.Wrap;
            }
            else
                aBatch.Begin();
            aBatch.Draw(myTexture, destRect, sourceRect, Target.Color, MathHelper.ToRadians(Target.Rotation),
               origin, Effects, 0.0f);
            aBatch.End();
        }

        public static bool IsAnimFile(string asFileName)
        {
            int numberSeparator = asFileName.LastIndexOf("_");
            int numDigits = asFileName.Length - numberSeparator - 1;

            bool bValidNumber = true;
            // So you're saying I've got a chance?
            if (numberSeparator > 0 && numDigits > 0)
            {
                // Now see if all of the digits between _ and . are numbers (i.e. test_001.jpg).
                for (int i = 1; i <= numDigits; ++i)
                {
                    if (!Char.IsDigit(asFileName[numberSeparator + i]))
                    {
                        bValidNumber = false;
                        break;
                    }
                }
            }

            return !(numberSeparator == -1 || numDigits <= 0 || !bValidNumber);
        }
    }
}
