﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using AngelMono.Infrastructure;

namespace AngelMono.Rendering
{
    public abstract class TextureRenderPath : IRenderPath
    {
        public abstract Texture2D CurrentTexture { get; }
        
        public virtual Rectangle? SourceRect 
        { 
            get { return null; } 
        }
        
        public virtual Vector2 Origin 
        {
            get
            {
                Texture2D current = CurrentTexture;
                return new Vector2(current.Width / 2, current.Height / 2);
            }
        }

        public SpriteEffects Effects
        {
            get;
            set;
        }

        public abstract void Update(GameTime aTime);
        public abstract void Render(GameTime aTime, Camera aCamera, GraphicsDevice aDevice, SpriteBatch aBatch);
    }
}
