﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AngelMono.Infrastructure.Console;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using AngelMono.Infrastructure;
using AngelMono.Actors;

namespace AngelMono.Rendering
{
    [ConsoleEnum]
    public enum AnimationType
    {
        None,
        Loop,
        PingPong,
        OneShot
    };

    public abstract class AnimRenderPath : TextureRenderPath
    {
        public virtual Actor Target
        {
            get;
            set;
        }

        [ConsoleMethod]
        public virtual void PlayAnimation(string asName)
        {
            throw new InvalidOperationException("This render path does not support named animations.");
        }

        [ConsoleMethod]
        public virtual void PlayAnimation(float afDelay, AnimationType aeType)
        {
            PlayAnimation(afDelay, aeType, null);
        }

        [ConsoleMethod]
        public abstract void PlayAnimation();

        [ConsoleMethod]
        public abstract void PlayAnimation(float afDelay, AnimationType aeType, string asName);
    }
}
