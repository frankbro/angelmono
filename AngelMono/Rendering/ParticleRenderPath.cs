﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AngelMono.Actors;
using Microsoft.Xna.Framework;
using AngelMono.Infrastructure;
using Microsoft.Xna.Framework.Graphics;
using System.ComponentModel;
using AngelMono.Editor;

#if WINDOWS
using System.Drawing.Design;
#endif

namespace AngelMono.Rendering
{
    /// <summary>
    /// Particle render path is a composite render path, it makes use
    /// of a ITextureRenderPath to figure out how to render
    /// </summary>
    public class ParticleRenderPath : AnimRenderPath
    {
        protected ParticleActor _target;
        protected TextureRenderPath _basisRenderer;

        public TextureRenderPath Basis
        {
            get { return _basisRenderer; }
        }

        public override Texture2D CurrentTexture
        {
            get { return _basisRenderer.CurrentTexture; }
        }

        public ParticleRenderPath(ParticleActor actor, TextureRenderPath basis)
        {
            _target = actor;
            _basisRenderer = basis;
        }

        public override void PlayAnimation()
        {
            AnimRenderPath basisAnim = _basisRenderer as AnimRenderPath;
            if (basisAnim != null)
                basisAnim.PlayAnimation();
        }

        public override void PlayAnimation(float afDelay, AnimationType aeType, string asAnimName)
        {
            AnimRenderPath basisAnim = _basisRenderer as AnimRenderPath;
            if (basisAnim != null)
                basisAnim.PlayAnimation(afDelay, aeType, asAnimName);
        }

        public override void Update(GameTime aTime)
        {
            _basisRenderer.Update(aTime);
        }

        public override void Render(GameTime atime, Camera aCamera, GraphicsDevice aDevice, SpriteBatch aBatch)
        {
            if (_target.Particles == null)
                return;

            Texture2D myTexture = _basisRenderer.CurrentTexture;

            aBatch.Begin();
            for (int i = 0; i < _target.Particles.Length; ++i)
            {
                if (_target.Particles[i]._age < 0.0f)
                    continue;

                Vector2 screenPos = aCamera.WorldToScreen(_target.Particles[i]._pos);
                Vector2 currentSize = new Vector2(_target.Particles[i]._scale, _target.Particles[i]._scale);
                Vector2 screenSize = aCamera.WorldSizeToScreenSize(currentSize);
                Rectangle destRect = new Rectangle((int)screenPos.X, (int)screenPos.Y, (int)screenSize.X, (int)screenSize.Y);

                aBatch.Draw(myTexture, destRect, _basisRenderer.SourceRect, _target.Particles[i]._color, 
                    _target.Particles[i]._rotation, _basisRenderer.Origin, _basisRenderer.Effects, 0.0f);
            }
            aBatch.End();
        }

        public override string ToString()
        {
            return String.Format("Particles: {0}", Basis.ToString());
        }
    }
}
