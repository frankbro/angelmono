﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using AngelMono.Infrastructure;
using Microsoft.Xna.Framework;
using AngelMono.Actors;
using AngelMono.Infrastructure.Console;
using AngelMono.Util;

namespace AngelMono.Rendering
{
    public class DefaultActorRenderPath : TextureRenderPath
    {        
        private static Texture2D _s_defaultTexture;

        protected Actor _target;

        public Actor Target
        {
            get { return _target; }
        }

        public static Texture2D s_defaultTexture
        {
            get
            {
                if (_s_defaultTexture == null)
                    _s_defaultTexture = World.Instance.Game.Content.Load<Texture2D>("white");
                return _s_defaultTexture;
            }
        }


        public override Texture2D CurrentTexture
        {
            get 
            {
                return s_defaultTexture; 
            }
        }

        public DefaultActorRenderPath(Actor target)
        {
            _target = target;
        }

        public override void Update(GameTime aTime)
        {
            
        }

        public override void Render(GameTime aTime, Camera aCamera, GraphicsDevice aDevice, SpriteBatch aBatch)
        {
            Vector2 screenPos = aCamera.WorldToScreen(_target.Position);
            Vector2 screenSize = aCamera.WorldSizeToScreenSize(_target.Size);
            Rectangle destRect = new Rectangle((int)screenPos.X, (int)screenPos.Y, (int)screenSize.X, (int)screenSize.Y);

            aBatch.Begin();
            aBatch.Draw(CurrentTexture, destRect, SourceRect, _target.Color, MathHelper.ToRadians(_target.Rotation),
               Origin, SpriteEffects.None, 0.0f);
            aBatch.End(); 
        }
    }
}
