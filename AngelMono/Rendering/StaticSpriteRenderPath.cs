﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using AngelMono.Infrastructure;
using Microsoft.Xna.Framework.Graphics;
using AngelMono.Actors;
using System.ComponentModel;
using AngelMono.Infrastructure.Console;
using AngelMono.Editor;


namespace AngelMono.Rendering
{
    public class StaticSpriteRenderPath : TextureRenderPath
    {
        protected Actor _target;
        
        protected string _spriteFile;
        protected Texture2D _sprite;

        [ConsoleProperty]
        [TypeConverter(typeof(ContentConverter))]
        public string Sprite
        {
            get { return _spriteFile; }
            set { SetSprite(value); }
        }

        public override Texture2D CurrentTexture
        {
            get 
            {
                if (_sprite == null)
                    _sprite = ContentHelpers.LoadTexture("white");
                return _sprite; 
            }
        }

        public bool TileSprite { get; set; }

        public StaticSpriteRenderPath(Actor target)
        {
            _target = target;
        }

        public StaticSpriteRenderPath(Actor target, string spriteFile)
        {
            _target = target;
            SetSprite(spriteFile);
        }

        public void SetSprite(string spriteFile)
        {
            _spriteFile = spriteFile;
            _sprite = ContentHelpers.LoadTexture(spriteFile);
        }

        public override void Update(GameTime aTime)
        {
            
        }

        public override void Render(GameTime aTime, Camera aCamera, GraphicsDevice aDevice, SpriteBatch aBatch)
        {
            if(_sprite == null)
                _sprite = ContentHelpers.LoadTexture("white");

            Vector2 screenPos = aCamera.WorldToScreen(_target.Position);
            Vector2 screenSize = aCamera.WorldSizeToScreenSize(_target.Size);
            Rectangle destRect = new Rectangle((int)screenPos.X, (int)screenPos.Y, (int)screenSize.X, (int)screenSize.Y);

            Rectangle? sourceRect = null;
            Vector2 origin = new Vector2(_sprite.Width / 2, _sprite.Height / 2);
            if (TileSprite)
            {
                float ftilesX = screenSize.X / _sprite.Width;
                float ftilesY = screenSize.Y / _sprite.Height;
                sourceRect = new Rectangle(0, 0, (int)(_sprite.Width * ftilesX), (int)(_sprite.Height * ftilesY));
                origin = new Vector2(sourceRect.Value.Width / 2, sourceRect.Value.Height / 2);
            }

            if (TileSprite)
            {
                aBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
                aDevice.SamplerStates[0].AddressU = TextureAddressMode.Wrap;
                aDevice.SamplerStates[0].AddressV = TextureAddressMode.Wrap;
            }
            else
                aBatch.Begin();
            aBatch.Draw(_sprite, destRect, sourceRect, _target.Color, MathHelper.ToRadians(_target.Rotation),
               origin, Effects, 0.0f);
            aBatch.End();
        }
    }
}
