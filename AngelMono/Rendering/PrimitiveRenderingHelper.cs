﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using AngelMono.Infrastructure;

namespace AngelMono.Rendering
{
    public class PrimitiveRenderingHelper
    {
        private static Texture2D s_SimpleTexture;

        public static void DrawLine(Camera aCamera, GraphicsDevice aDevice, SpriteBatch aBatch,
            Vector2 aTo, Vector2 aFrom, Color aColor)
        {
            if (s_SimpleTexture == null)
                Initialize(aDevice);

            Vector2 screenFrom = aCamera.WorldToScreen(aFrom);
            Vector2 screenTo = aCamera.WorldToScreen(aTo);

            int distance = (int)Vector2.Distance(screenFrom, screenTo);

            float angle = (float)Math.Atan2((double)(screenTo.Y - screenFrom.Y),
                    (double)(screenTo.X - screenFrom.X));

            aBatch.Begin();
            aBatch.Draw(s_SimpleTexture, screenFrom, new Rectangle(0, 0, 1, 1), aColor, angle, Vector2.Zero, 
                new Vector2(distance, 1), SpriteEffects.None, 0);
            aBatch.End();
        }

        private static void Initialize(GraphicsDevice aDevice)
        {
            s_SimpleTexture = World.Instance.Game.Content.Load<Texture2D>("white");
        }
    }
}
