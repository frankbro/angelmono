﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using AngelMono.Infrastructure;
using System.ComponentModel;
using AngelMono.Editor;
using AngelMono.Infrastructure.Console;
using AngelMono.Actors;

namespace AngelMono.Rendering
{
    public class TiledSpriteRenderPath : TextureRenderPath
    {
        private string _sFileName;
        private Texture2D _TileMap;

        private Vector2 _FrameSize;
        private Vector2 _CurrentFrame;

        public virtual Actor Target
        {
            get;
            set;
        }

        [ConsoleProperty]
        public Vector2 CurrentFrame
        {
            get { return _CurrentFrame; }
            set { _CurrentFrame = value; }
        }

        [ConsoleProperty]
        public Vector2 FrameSize
        {
            get { return _FrameSize; }
            set { _FrameSize = value; }
        }

        [ConsoleProperty]
        [TypeConverter(typeof(ContentConverter))]
        public string File
        {
            get { return _sFileName; }
            set
            {
                LoadTileset(value);
            }
        }

        public override Texture2D CurrentTexture
        {
            get
            {
                return _TileMap;
            }
        }

        public override Rectangle? SourceRect
        {
            get
            {
                return new Rectangle((int)(_CurrentFrame.X * _FrameSize.X),
                    (int)(_CurrentFrame.Y * _FrameSize.Y), (int)_FrameSize.X, (int)_FrameSize.Y);
            }
        }

        public override Vector2 Origin
        {
            get
            {
                return new Vector2(_FrameSize.X / 2, _FrameSize.Y / 2);
            }
        }

        public TiledSpriteRenderPath(Actor target)
        {
            Target = target;
            _TileMap = ContentHelpers.LoadTexture("white");
        }

        public TiledSpriteRenderPath(Actor target, string asFileName)
        {
            Target = target;
            LoadTileset(asFileName);
        }

        public void LoadTileset(string asFileName)
        {
            _sFileName = asFileName;
            _TileMap = World.Instance.Game.Content.Load<Texture2D>(_sFileName);

            if (TileInfoCache.HasTileInfo(asFileName))
            {
                TileInfo info = TileInfoCache.Instance.GetForFile(asFileName);
                FrameSize = info.FrameSize;
            }
        }

        public override void Update(GameTime aTime)
        {
            
        }

        public override void Render(GameTime aTime, Camera aCamera, GraphicsDevice aDevice, SpriteBatch aBatch)
        {
            Vector2 screenPos = aCamera.WorldToScreen(Target.Position);
            Vector2 screenSize = aCamera.WorldSizeToScreenSize(Target.Size);
            Rectangle destRect = new Rectangle((int)screenPos.X, (int)screenPos.Y, (int)screenSize.X, (int)screenSize.Y);

            aBatch.Begin();
            aBatch.Draw(_TileMap, destRect, SourceRect, Target.Color, MathHelper.ToRadians(Target.Rotation),
               Origin, Effects, 0.0f);
            aBatch.End();
        }

        [ConsoleMethod]
        public static TiledAnimRenderPath Create()
        {
            return new TiledAnimRenderPath();
        }
    }
}
