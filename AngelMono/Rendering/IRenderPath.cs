﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using AngelMono.Infrastructure;
using System.ComponentModel;
using AngelMono.Editor;

namespace AngelMono.Rendering
{
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public interface IRenderPath
    {
        void Update(GameTime aTime);
        void Render(GameTime aTime, Camera aCamera, GraphicsDevice aDevice, SpriteBatch aBatch);
    }
}
