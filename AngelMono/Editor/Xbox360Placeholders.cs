using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AngelMono.Editor
{
#if !WINDOWS
    public class EditorAttribute : Attribute
    {
        public EditorAttribute(Type type1, Type type2)
        {

        }
    }

    public class TypeConverterAttribute : Attribute
    {
        public TypeConverterAttribute(Type type)
        {

        }
    }

    public class CategoryAttribute : Attribute
    {
        public CategoryAttribute(string category)
        {

        }
    }

    public class ExpandableObjectConverter
    {

    }

    public class UITypeEditor
    {

    }
#endif
}