﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Reflection;
using AngelMono.Infrastructure;
using AngelMono.Actors;

namespace AngelMono.Editor
{
#if WINDOWS
    using System.ComponentModel.Design;
    using System.Windows.Forms;
	
    public class AllOfTypeObjectSelector<T> : ObjectSelectorEditor
    {
        private static Type[] s_OnlyActor = new Type[] { typeof(Actor) };

        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            object newValue = base.EditValue(context, provider, value);

            if(prevValue == currValue)
                return currValue;       // This means we're displaying the current object, don't worry about it.

            // If the type has not changed, return the previous object.
            Type currType = (Type)currValue;
            if (prevValue.GetType() == (Type)currValue)
            {
                currValue = prevValue;
                return prevValue;
            }

            // Type has changed, create a new object for the type
            ConstructorInfo constructor = currType.GetConstructor(Type.EmptyTypes);
            if(constructor != null)
                return constructor.Invoke(null);

            // See if there's one that accepts an actor as it's only parameter
            constructor = currType.GetConstructor(s_OnlyActor);
            if(constructor == null)
                throw new Exception("Type used in AllOfTypeObjectSelector does not support a default / actor constructor!");
            
            return constructor.Invoke(new object[] {context.Instance});
        }

        protected override void FillTreeWithData(ObjectSelectorEditor.Selector selector, ITypeDescriptorContext context, IServiceProvider provider)
        {
            // Clears tree
            base.FillTreeWithData(selector, context, provider);

            object currentValue = context.PropertyDescriptor.GetValue(context.Instance);
            
            // Get all types that inherit from my base ttype
            List<Type> types = GetTypes(Assembly.GetExecutingAssembly());
            types.AddRange(GetTypes(World.Instance.Game.GetType().Assembly));

            foreach(Type type in types)
            {
                SelectorNode node = new SelectorNode(type.Name, type);
                selector.Nodes.Add(node);

                if (currentValue != null && currentValue.GetType() == type)
                    selector.SelectedNode = node;
            }
        }

        private List<Type> GetTypes(Assembly assembly)
        {
            return assembly.GetTypes().Where(type => (
                type.IsClass && typeof(T).IsAssignableFrom(type) &&
                (type.GetConstructor(Type.EmptyTypes) != null || type.GetConstructor(s_OnlyActor) != null)
            )).ToList();
        }
    }
#else
    public class AllOfTypeObjectSelector<T>
    {

    }
#endif
}