﻿using System.ComponentModel;
using AngelMono.Infrastructure;

namespace AngelMono.Editor
{
#if WINDOWS
    public class FontConverter : StringConverter
    {
        public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
        {
            return true;
        }

        public override TypeConverter.StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
        {
            return new StandardValuesCollection(FontCache.Instance.GetRegisteredFonts());
        }
    }
#else
    public class FontConverter
    {

    }
#endif
}