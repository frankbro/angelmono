﻿#if WINDOWS

namespace AngelMono.Editor
{
    partial class EditorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._pgObjProperties = new System.Windows.Forms.PropertyGrid();
            this.btnSaveActorDefinition = new System.Windows.Forms.Button();
            this._lblActorDefs = new System.Windows.Forms.Label();
            this._btnAddActor = new System.Windows.Forms.Button();
            this._tcActorDefinitions = new System.Windows.Forms.TabControl();
            this._tpTemplates = new System.Windows.Forms.TabPage();
            this._lbActorDefinitions = new System.Windows.Forms.ListBox();
            this.cmAddActor = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miAddToLevel = new System.Windows.Forms.ToolStripMenuItem();
            this.miAddToWorld = new System.Windows.Forms.ToolStripMenuItem();
            this.miAddToUI = new System.Windows.Forms.ToolStripMenuItem();
            this._tpBaseActors = new System.Windows.Forms.TabPage();
            this._lbBaseActors = new System.Windows.Forms.ListBox();
            this._ddlSimulate = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miNew = new System.Windows.Forms.ToolStripMenuItem();
            this.mi_Save = new System.Windows.Forms.ToolStripMenuItem();
            this.mi_saveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.chkShowUI = new System.Windows.Forms.CheckBox();
            this._tcActorDefinitions.SuspendLayout();
            this._tpTemplates.SuspendLayout();
            this.cmAddActor.SuspendLayout();
            this._tpBaseActors.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _pgObjProperties
            // 
            this._pgObjProperties.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._pgObjProperties.Location = new System.Drawing.Point(12, 61);
            this._pgObjProperties.Name = "_pgObjProperties";
            this._pgObjProperties.Size = new System.Drawing.Size(349, 375);
            this._pgObjProperties.TabIndex = 0;
            this._pgObjProperties.UseCompatibleTextRendering = true;
            // 
            // btnSaveActorDefinition
            // 
            this.btnSaveActorDefinition.Location = new System.Drawing.Point(12, 442);
            this.btnSaveActorDefinition.Name = "btnSaveActorDefinition";
            this.btnSaveActorDefinition.Size = new System.Drawing.Size(131, 23);
            this.btnSaveActorDefinition.TabIndex = 1;
            this.btnSaveActorDefinition.Text = "Save Actor Definition";
            this.btnSaveActorDefinition.UseVisualStyleBackColor = true;
            this.btnSaveActorDefinition.Click += new System.EventHandler(this.btnSaveActorDefinition_Click);
            // 
            // _lblActorDefs
            // 
            this._lblActorDefs.AutoSize = true;
            this._lblActorDefs.Location = new System.Drawing.Point(9, 481);
            this._lblActorDefs.Name = "_lblActorDefs";
            this._lblActorDefs.Size = new System.Drawing.Size(130, 13);
            this._lblActorDefs.TabIndex = 3;
            this._lblActorDefs.Text = "Available Actor Definitions";
            // 
            // _btnAddActor
            // 
            this._btnAddActor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._btnAddActor.Location = new System.Drawing.Point(15, 724);
            this._btnAddActor.Name = "_btnAddActor";
            this._btnAddActor.Size = new System.Drawing.Size(75, 23);
            this._btnAddActor.TabIndex = 4;
            this._btnAddActor.Text = "Add Actor";
            this._btnAddActor.UseVisualStyleBackColor = true;
            this._btnAddActor.Click += new System.EventHandler(this._btnAddActor_Click);
            // 
            // _tcActorDefinitions
            // 
            this._tcActorDefinitions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._tcActorDefinitions.Controls.Add(this._tpTemplates);
            this._tcActorDefinitions.Controls.Add(this._tpBaseActors);
            this._tcActorDefinitions.Location = new System.Drawing.Point(12, 497);
            this._tcActorDefinitions.Name = "_tcActorDefinitions";
            this._tcActorDefinitions.SelectedIndex = 0;
            this._tcActorDefinitions.Size = new System.Drawing.Size(349, 221);
            this._tcActorDefinitions.TabIndex = 5;
            // 
            // _tpTemplates
            // 
            this._tpTemplates.Controls.Add(this._lbActorDefinitions);
            this._tpTemplates.Location = new System.Drawing.Point(4, 22);
            this._tpTemplates.Name = "_tpTemplates";
            this._tpTemplates.Padding = new System.Windows.Forms.Padding(3);
            this._tpTemplates.Size = new System.Drawing.Size(341, 195);
            this._tpTemplates.TabIndex = 0;
            this._tpTemplates.Text = "Actor Templates";
            this._tpTemplates.UseVisualStyleBackColor = true;
            // 
            // _lbActorDefinitions
            // 
            this._lbActorDefinitions.ContextMenuStrip = this.cmAddActor;
            this._lbActorDefinitions.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lbActorDefinitions.FormattingEnabled = true;
            this._lbActorDefinitions.Location = new System.Drawing.Point(3, 3);
            this._lbActorDefinitions.Name = "_lbActorDefinitions";
            this._lbActorDefinitions.Size = new System.Drawing.Size(335, 186);
            this._lbActorDefinitions.TabIndex = 3;
            // 
            // cmAddActor
            // 
            this.cmAddActor.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miAddToLevel,
            this.miAddToWorld,
            this.miAddToUI});
            this.cmAddActor.Name = "cmAddActor";
            this.cmAddActor.Size = new System.Drawing.Size(176, 70);
            // 
            // miAddToLevel
            // 
            this.miAddToLevel.Name = "miAddToLevel";
            this.miAddToLevel.Size = new System.Drawing.Size(175, 22);
            this.miAddToLevel.Text = "Add To Level";
            this.miAddToLevel.Click += new System.EventHandler(this.miAddToLevel_Click);
            // 
            // miAddToWorld
            // 
            this.miAddToWorld.Name = "miAddToWorld";
            this.miAddToWorld.Size = new System.Drawing.Size(175, 22);
            this.miAddToWorld.Text = "Add To World Only";
            this.miAddToWorld.Click += new System.EventHandler(this.miAddToWorld_Click);
            // 
            // miAddToUI
            // 
            this.miAddToUI.Name = "miAddToUI";
            this.miAddToUI.Size = new System.Drawing.Size(175, 22);
            this.miAddToUI.Text = "Add To UI";
            this.miAddToUI.Click += new System.EventHandler(this.miAddToUI_Click);
            // 
            // _tpBaseActors
            // 
            this._tpBaseActors.Controls.Add(this._lbBaseActors);
            this._tpBaseActors.Location = new System.Drawing.Point(4, 22);
            this._tpBaseActors.Name = "_tpBaseActors";
            this._tpBaseActors.Padding = new System.Windows.Forms.Padding(3);
            this._tpBaseActors.Size = new System.Drawing.Size(341, 195);
            this._tpBaseActors.TabIndex = 1;
            this._tpBaseActors.Text = "Base Actors";
            this._tpBaseActors.UseVisualStyleBackColor = true;
            // 
            // _lbBaseActors
            // 
            this._lbBaseActors.ContextMenuStrip = this.cmAddActor;
            this._lbBaseActors.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lbBaseActors.FormattingEnabled = true;
            this._lbBaseActors.Location = new System.Drawing.Point(3, 3);
            this._lbBaseActors.Name = "_lbBaseActors";
            this._lbBaseActors.Size = new System.Drawing.Size(335, 186);
            this._lbBaseActors.TabIndex = 0;
            // 
            // _ddlSimulate
            // 
            this._ddlSimulate.FormattingEnabled = true;
            this._ddlSimulate.Items.AddRange(new object[] {
            "On",
            "Off"});
            this._ddlSimulate.Location = new System.Drawing.Point(76, 37);
            this._ddlSimulate.Name = "_ddlSimulate";
            this._ddlSimulate.Size = new System.Drawing.Size(67, 21);
            this._ddlSimulate.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Simulation:";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(373, 24);
            this.menuStrip1.TabIndex = 8;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miNew,
            this.mi_Save,
            this.mi_saveAs});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // miNew
            // 
            this.miNew.Name = "miNew";
            this.miNew.Size = new System.Drawing.Size(152, 22);
            this.miNew.Text = "&New";
            this.miNew.Click += new System.EventHandler(this.miNew_Click);
            // 
            // mi_Save
            // 
            this.mi_Save.Name = "mi_Save";
            this.mi_Save.Size = new System.Drawing.Size(152, 22);
            this.mi_Save.Text = "&Save";
            this.mi_Save.Click += new System.EventHandler(this.mi_save_Click);
            // 
            // mi_saveAs
            // 
            this.mi_saveAs.Name = "mi_saveAs";
            this.mi_saveAs.Size = new System.Drawing.Size(152, 22);
            this.mi_saveAs.Text = "Save &As";
            this.mi_saveAs.Click += new System.EventHandler(this.mi_saveAs_Click);
            // 
            // chkShowUI
            // 
            this.chkShowUI.AutoSize = true;
            this.chkShowUI.Location = new System.Drawing.Point(274, 39);
            this.chkShowUI.Name = "chkShowUI";
            this.chkShowUI.Size = new System.Drawing.Size(67, 17);
            this.chkShowUI.TabIndex = 9;
            this.chkShowUI.Text = "Show UI";
            this.chkShowUI.UseVisualStyleBackColor = true;
            this.chkShowUI.CheckedChanged += new System.EventHandler(this.chkShowUI_CheckedChanged);
            // 
            // EditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(373, 793);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._ddlSimulate);
            this.Controls.Add(this.chkShowUI);
            this.Controls.Add(this._tcActorDefinitions);
            this.Controls.Add(this._btnAddActor);
            this.Controls.Add(this._lblActorDefs);
            this.Controls.Add(this.btnSaveActorDefinition);
            this.Controls.Add(this._pgObjProperties);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "EditorForm";
            this.Text = "AngelMono Edit Window";
            this._tcActorDefinitions.ResumeLayout(false);
            this._tpTemplates.ResumeLayout(false);
            this.cmAddActor.ResumeLayout(false);
            this._tpBaseActors.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PropertyGrid _pgObjProperties;
        private System.Windows.Forms.Button btnSaveActorDefinition;
        private System.Windows.Forms.Label _lblActorDefs;
        private System.Windows.Forms.Button _btnAddActor;
        private System.Windows.Forms.TabControl _tcActorDefinitions;
        private System.Windows.Forms.TabPage _tpTemplates;
        private System.Windows.Forms.ListBox _lbActorDefinitions;
        private System.Windows.Forms.TabPage _tpBaseActors;
        private System.Windows.Forms.ListBox _lbBaseActors;
        private System.Windows.Forms.ComboBox _ddlSimulate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mi_Save;
        private System.Windows.Forms.ToolStripMenuItem mi_saveAs;
        private System.Windows.Forms.ContextMenuStrip cmAddActor;
        private System.Windows.Forms.ToolStripMenuItem miAddToLevel;
        private System.Windows.Forms.ToolStripMenuItem miAddToWorld;
        private System.Windows.Forms.ToolStripMenuItem miAddToUI;
        private System.Windows.Forms.ToolStripMenuItem miNew;
        private System.Windows.Forms.CheckBox chkShowUI;
    }
}

#endif