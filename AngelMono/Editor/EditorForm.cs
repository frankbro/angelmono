﻿#if WINDOWS

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AngelMono.Infrastructure;
using AngelMono.Input;
using Microsoft.Xna.Framework;
using AngelMono.Actors;
using AngelMono.Physics;
using System.Globalization;
using System.IO;
using System.Reflection;
using AngelMono.Infrastructure.Console;
using Microsoft.Xna.Framework.Input;

namespace AngelMono.Editor
{
    public partial class EditorForm : Form, IMouseListener, IKeyListener
    {
        private Renderable _selectedObject;
        private Vector2 _HeldOffset;

        private bool _bObjectRotating = false;
        private bool _bObjectScalingVertically = false;
        private bool _bObjectScalingHorizontally = false;
        private bool _bObjectHeld = false;
        private bool _bShouldClone = false;
        private bool _bCameraHeld = false;
        private Vector2 _screenHeldPosition;
        private Vector3 _cameraHeldPosition;
        private bool _bPhysicsActorWasInitialized = false;

        Dictionary<string, ConstructorInfo> _baseActorMap = new Dictionary<string, ConstructorInfo>();

        public EditorForm()
        {
            InitializeComponent();

            this.Disposed += new EventHandler(EditorForm_Disposed);
            this.Shown += new EventHandler(EditorForm_Shown);

            chkShowUI.Checked = UI.Instance.Show;

            _ddlSimulate.SelectedItem = World.Instance.IsSimulating ? "On" : "Off";
            _ddlSimulate.SelectedIndexChanged += new EventHandler(_ddlSimulate_SelectedIndexChanged);

            InputManager.Instance.RegisterKeyListerer(this);

            RebindActorDefinitions();
            RebindBaseActors();
        }

        void _ddlSimulate_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((string)_ddlSimulate.SelectedItem) == "On")
                World.Instance.IsSimulating = true;
            else
                World.Instance.IsSimulating = false;
        }

        public Renderable Selected
        {
            get { return _selectedObject; }
            set
            {
                if (value != _selectedObject)
                {
                    OnObjectDeselect();
                    _selectedObject = value;
                    OnSelectedObjectChanged();
                }
            }
        }

        public void RebindActorDefinitions()
        {
            string dir = ActorFactory.Instance.ActorTemplateDirectory;
            if (Directory.Exists(dir))
            {
                DirectoryInfo dirInfo = new DirectoryInfo(dir);
                FileInfo[] files = dirInfo.GetFiles("*.adf", SearchOption.AllDirectories);

                string[] definitions = new string[files.Length];
                for (int i = 0; i < files.Length; ++i)
                {
                    definitions[i] = Path.GetFileNameWithoutExtension(files[i].Name);
                }
                Array.Sort(definitions);

                _lbActorDefinitions.DataSource = definitions;
            }
        }

        public void RebindBaseActors()
        {
            BindBaseActorAssembly(Assembly.GetExecutingAssembly());
            BindBaseActorAssembly(World.Instance.Game.GetType().Assembly);

            string[] baseActorNames = _baseActorMap.Keys.ToArray();
            Array.Sort(baseActorNames);
            _lbBaseActors.DataSource = baseActorNames;
        }

        private void BindBaseActorAssembly(Assembly assm)
        {
            foreach (Type type in assm.GetExportedTypes())
            {
                if (typeof(Actor).IsAssignableFrom(type))
                {
                    // See if it has a default constructor
                    ConstructorInfo info = type.GetConstructor(Type.EmptyTypes);
                    if (info != null)
                        _baseActorMap.Add(type.Name, info);
                }
            }
        }

        private void OnObjectDeselect()
        {
            ConsoleVariable cvar = DeveloperConsole.Instance.ItemManager.GetCVar("selectedObject");
            cvar.Value = null;

            if (_bPhysicsActorWasInitialized)
            {
                IPhysicalObject phys = _selectedObject as IPhysicalObject;
                if (phys != null)
                    phys.InitPhysics(); // Reinitialize physics with current values

                _bPhysicsActorWasInitialized = false;
            }
        }

        public void AddActor(Actor actor, bool toLevel)
        {
            // Find the center of the screen
            Camera cam = World.Instance.Camera;
            Vector2 centerScreen = cam.ScreenToWorld(cam.WindowWidth / 2, cam.WindowHeight / 2);
            actor.Position = centerScreen;

            if (toLevel)
                World.Instance.CurrentLevel.AddActor(actor);
            else
                World.Instance.Add(actor);
            Selected = actor;
        }

        public void AddActorToUI(Actor actor)
        {
            // Find the center of the screen
            Camera cam = UI.Instance.Camera;
            Vector2 centerScreen = cam.ScreenToWorld(cam.WindowWidth / 2, cam.WindowHeight / 2);
            actor.Position = centerScreen;

            UI.Instance.Add(actor);
            Selected = actor;
        }

        private void OnSelectedObjectChanged()
        {
            // Special case for physics actors.  Remove them from the world before manipulating them
            IPhysicalObject physActor = Selected as IPhysicalObject;
            if (physActor != null && physActor.IsInitialized)
            {
                physActor.DestroyPhysics();
                _bPhysicsActorWasInitialized = true;
            }

            ConsoleVariable cvar = DeveloperConsole.Instance.ItemManager.GetCVar("selectedObject");
            cvar.Value = Selected;

            // This is to prevent a problem with closing the form.  If both
            // the form and the selected object are null, don't change either
            if(_pgObjProperties.SelectedObject != null || Selected != null)
                _pgObjProperties.SelectedObject = Selected;
        }

        private void SaveLevel()
        {
            if (World.Instance.CurrentLevel.LevelFile != null)
                World.Instance.CurrentLevel.Save();
            else
                SaveLevelAs();
        }

        private void SaveLevelAs()
        {
            InputDialog dialog = new InputDialog("Level Name", null);
            DialogResult result = dialog.ShowDialog(this);
            if (result == DialogResult.OK && dialog.Value.Length > 0)
                World.Instance.CurrentLevel.SaveAs(dialog.Value);
        }

        private void EditorForm_Disposed(object sender, EventArgs e)
        {
            Selected = null;
            InputManager.Instance.UnregisterMouseListener(this);
            InputManager.Instance.UnregisterKeyListener(this);
            World.Instance.EndEditing();
        }

        void EditorForm_Shown(object sender, EventArgs e)
        {
            InputManager.Instance.RegisterMouseListener(this);
        }

        private void miNew_Click(object sender, EventArgs e)
        {
            World.Instance.CurrentLevel.Clear();
        }

        private void mi_save_Click(object sender, EventArgs e)
        {
            SaveLevel();
        }

        private void mi_saveAs_Click(object sender, EventArgs e)
        {
            SaveLevelAs();
        }

        private void miAddToLevel_Click(object sender, EventArgs e)
        {
            Actor theActor;
            if (_tcActorDefinitions.SelectedTab == _tpBaseActors)
            {
                ConstructorInfo info = _baseActorMap[(string)_lbBaseActors.SelectedItem];
                theActor = (Actor)info.Invoke(null);
            }
            else
            {
                string definition = (string)_lbActorDefinitions.SelectedItem;
                theActor = ActorFactory.Instance.CreateActor(definition);
            }

            AddActor(theActor, true);
        }

        private void miAddToWorld_Click(object sender, EventArgs e)
        {
            Actor theActor;
            if (_tcActorDefinitions.SelectedTab == _tpBaseActors)
            {
                ConstructorInfo info = _baseActorMap[(string)_lbBaseActors.SelectedItem];
                theActor = (Actor)info.Invoke(null);
            }
            else
            {
                string definition = (string)_lbActorDefinitions.SelectedItem;
                theActor = ActorFactory.Instance.CreateActor(definition);
            }

            AddActor(theActor, false);
        }

        private void miAddToUI_Click(object sender, EventArgs e)
        {
            Actor theActor;
            if (_tcActorDefinitions.SelectedTab == _tpBaseActors)
            {
                ConstructorInfo info = _baseActorMap[(string)_lbBaseActors.SelectedItem];
                theActor = (Actor)info.Invoke(null);
            }
            else
            {
                string definition = (string)_lbActorDefinitions.SelectedItem;
                theActor = ActorFactory.Instance.CreateActor(definition);
            }

            AddActorToUI(theActor);
        }

        private void btnSaveActorDefinition_Click(object sender, EventArgs e)
        {
            Actor selectedActor = Selected as Actor;
            if (selectedActor != null)
            {
                InputDialog dialog = new InputDialog("Actor Definition Name", selectedActor.ActorDefinition);
                DialogResult result = dialog.ShowDialog(this);
                if (result == DialogResult.OK && dialog.Value.Length > 0)
                {
                    ActorFactory.Instance.SerializeActorDefinition(dialog.Value, selectedActor);
                    RebindActorDefinitions();
                }
            }
        }

        private void _btnAddActor_Click(object sender, EventArgs e)
        {
            Actor theActor;
            if (_tcActorDefinitions.SelectedTab == _tpBaseActors)
            {
                ConstructorInfo info = _baseActorMap[(string)_lbBaseActors.SelectedItem];
                theActor = (Actor)info.Invoke(null);
            }
            else
            {
                string definition = (string)_lbActorDefinitions.SelectedItem;
                theActor = ActorFactory.Instance.CreateActor(definition);
            }

            AddActor(theActor, true);
        }

        private void chkShowUI_CheckedChanged(object sender, EventArgs e)
        {
            UI.Instance.Show = chkShowUI.Checked;
        }

        #region IMouseListener Members

        public void MouseDownEvent(Vector2 screenCoordinates, InputManager.MouseButton button)
        {
            if (!this.Owner.Focused)
                return;

            if (button == InputManager.MouseButton.Right)
                return;

            _bObjectHeld = false;
            _bObjectRotating = false;
            _bObjectScalingHorizontally = false;
            _bObjectScalingVertically = false;
            _bCameraHeld = false;

            Selected = World.Instance.FindAt((int)screenCoordinates.X, (int)screenCoordinates.Y);
            if (Selected != null)
            {
                Actor selectedActor = Selected as Actor;
                if (_bShouldClone && selectedActor != null)
                {
                    using (StringWriter writer = new StringWriter())
                    {
                        ActorFactory.Instance.SerializeActor(writer, selectedActor);
                        Selected = (Renderable)DeveloperConsole.Instance.ExecuteInConsole(writer.ToString());
                    }
                }

                Vector2 worldCoordinates = World.Instance.Camera.ScreenToWorld((int)screenCoordinates.X, (int)screenCoordinates.Y);
                _HeldOffset = Selected.Position - worldCoordinates;
                Actor actor = (Selected as Actor);
                KeyboardState state = Keyboard.GetState();
                if (actor != null && (state.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.RightShift)
                    || state.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.LeftShift)))
                {
                    // Enter RotateMode
                    _bObjectRotating = true;
                    // Convert the offset to find where this should be to have an angle of zero.
                    float offsetLength = _HeldOffset.Length();
                    double angle = Math.Atan2(-_HeldOffset.Y, -_HeldOffset.X);
                    angle += Math.PI * actor.Rotation / 180;
                    _HeldOffset = new Vector2(offsetLength * (float)Math.Cos(angle), offsetLength * (float)Math.Sin(angle));
                }
                else if (actor != null && (state.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.RightAlt)
                    || state.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.LeftAlt)))
                {
                    // Enter Scale Mode
                    // Are you scaling vertically or horizontally?
                    float offsetLength = _HeldOffset.Length();
                    double angle = Math.Atan2(-_HeldOffset.Y, -_HeldOffset.X);
                    angle += Math.PI * actor.Rotation / 180;
                    Vector2 rotatedOffset = new Vector2(offsetLength * (float)Math.Cos(angle), offsetLength * (float)Math.Sin(angle));
                    if (Math.Abs(rotatedOffset.Y) > Math.Abs(rotatedOffset.X))
                    {
                        _bObjectScalingVertically = true;
                        offsetLength = actor.Size.Y / Math.Max(Math.Abs(rotatedOffset.Y), actor.Size.Y * 0.2f);
                        angle = Math.PI * (rotatedOffset.Y > 0 ? 0.5 : -0.5) - Math.PI * actor.Rotation / 180;
                        _HeldOffset = new Vector2(offsetLength * (float)Math.Cos(angle), offsetLength * (float)Math.Sin(angle));
                    }
                    else
                    {
                        _bObjectScalingHorizontally = true;
                        offsetLength = actor.Size.X / Math.Max(Math.Abs(rotatedOffset.X), actor.Size.X * 0.2f);
                        angle = (rotatedOffset.X > 0 ? 0 : Math.PI) - Math.PI * actor.Rotation / 180;
                        _HeldOffset = new Vector2(offsetLength * (float)Math.Cos(angle), offsetLength * (float)Math.Sin(angle));
                    }
                }
                else
                {
                    // Enter Translate Mode
                    _bObjectHeld = true;
                }
            }
            else
            {
                _bCameraHeld = true;
                _screenHeldPosition = screenCoordinates;
                _cameraHeldPosition = World.Instance.Camera.Position;
            }
        }

        public void MouseUpEvent(Vector2 screenCoordinates, InputManager.MouseButton button)
        {
            if (button == InputManager.MouseButton.Right)
                return;

            _bObjectHeld = false;
            _bObjectRotating = false;
            _bObjectScalingHorizontally = false;
            _bObjectScalingVertically = false;
            _bCameraHeld = false;
        }

        public void MouseWheelEvent(int amount)
        {
            Vector3 position = World.Instance.Camera.Position;
            position.Z += (amount / 100);
            if (position.Z > 1.0f)    // Don't let the zoom loop around
                World.Instance.Camera.Position = position;
        }

        public void MouseMotionEvent(int screenPosX, int screenPosY)
        {
            if (_bObjectHeld)
            {
                Vector2 worldCoordinates = World.Instance.Camera.ScreenToWorld(screenPosX, screenPosY);
                Selected.Position = worldCoordinates + _HeldOffset;
                _pgObjProperties.Refresh();
            }
            else if (_bObjectRotating)
            {
                Vector2 newOffset = World.Instance.Camera.ScreenToWorld(screenPosX, screenPosY) - Selected.Position;
                (Selected as Actor).Rotation = -180 * (float)((Math.Atan2(newOffset.Y, newOffset.X) - Math.Atan2(_HeldOffset.Y, _HeldOffset.X)) / Math.PI);
                _pgObjProperties.Refresh();
            }
            else if (_bObjectScalingVertically)
            {
                (Selected as Actor).Size = new Vector2((Selected as Actor).Size.X, 
                    Math.Abs(Vector2.Dot(_HeldOffset, World.Instance.Camera.ScreenToWorld(screenPosX, screenPosY) - Selected.Position)));
                _pgObjProperties.Refresh();
            }
            else if (_bObjectScalingHorizontally)
            {
                (Selected as Actor).Size = new Vector2(Math.Abs(Vector2.Dot(_HeldOffset, World.Instance.Camera.ScreenToWorld(screenPosX, screenPosY) - Selected.Position)),
                    (Selected as Actor).Size.Y);
                _pgObjProperties.Refresh();
            }
            else if (_bCameraHeld)
            {
                // Because of DirectX handedness, X is correct, but Y is backwords.  Trust me, this is correct.
                Vector2 screenOffset = new Vector2(_screenHeldPosition.X - screenPosX, screenPosY - _screenHeldPosition.Y);
                Vector2 worldOffset = World.Instance.Camera.ScreenSizeToWorldSize(screenOffset);
                World.Instance.Camera.Position = _cameraHeldPosition + new Vector3(worldOffset, 0.0f);
            }
        }

        #endregion

        #region IKeyListener Members

        public void OnKeyDown(Microsoft.Xna.Framework.Input.Keys key)
        {
#if WINDOWS
            if (String.Equals(InputManager.Instance.GetKeyBinding(key, true), "ToggleEditing"))
                World.Instance.ToggleEditing(); // This is stupid, since there is still a queued message that just immediately reopens the window
            if (String.Equals(InputManager.Instance.GetKeyBinding(key, true), "EndEditing"))
                World.Instance.EndEditing();
#endif

            if (key == Microsoft.Xna.Framework.Input.Keys.Delete && this.Owner.Focused)
            {
                if (_selectedObject != null)
                {
                    // Note: Selected = null must be called first so that physics actors
                    // can re-initialize then be removed from the world.
                    Renderable holdSelectedObject = _selectedObject;
                    Selected = null;
                    World.Instance.Remove(holdSelectedObject);
                }
            }
            if (key == Microsoft.Xna.Framework.Input.Keys.RightControl
                || key == Microsoft.Xna.Framework.Input.Keys.LeftControl)
            {
                _bShouldClone = true;
            }

            KeyboardState state = Keyboard.GetState();
            if (key == Microsoft.Xna.Framework.Input.Keys.S && 
                (state.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.RightControl)
                || state.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.LeftControl)))
            {
                SaveLevel();
            }
        }

        public void OnKeyUp(Microsoft.Xna.Framework.Input.Keys key)
        {
            if (key == Microsoft.Xna.Framework.Input.Keys.RightControl
                || key == Microsoft.Xna.Framework.Input.Keys.LeftControl)
            {
                _bShouldClone = false;
            }
        }

        #endregion
    }
}

#endif