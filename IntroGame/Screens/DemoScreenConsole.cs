﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AngelMono.Actors;
using AngelMono.Infrastructure.Console;
using Microsoft.Xna.Framework;
using AngelMono.Infrastructure;
using Microsoft.Xna.Framework.Graphics;

namespace IntroGame.Screens
{
    public class DemoScreenConsole : Screen
    {
        private Actor a;

        private TextActor t1;
        private TextActor t2;
        private TextActor t3;

        public override void Enter()
        {
            //Place the actor from a definition file
            a = ActorFactory.Instance.CreateActor("simple_actor");
            AddToScreen(a);

            // Definitions for the global namespace in the console are dirty, but
            // anything else should be nice and clean.
            DeveloperConsole.Instance.ItemManager.AddCommand("AddTexture", x => {
                a.SetSprite("Images\\angel");
                return null;
            });

            DeveloperConsole.Instance.ItemManager.AddCommand("ChangeSize", x => {
                DeveloperConsole.VerifyArgs(x, typeof(float));
                a.Size = new Vector2((float)x[0], (float)x[0]);
                return null;
            });

            t1 = new TextActor("Console", "This demo shows off the console.");
            t1.Position = new Vector2(0, -3.5f);
            t1.TextAlignment = TextActor.Alignment.Center;
            t2 = new TextActor("Console", "Press ~ to open it up. Execute \"AddTexture()\", enjoying the tab-completion.");
            t2.Position = new Vector2(0, -4.5f);
            t2.TextAlignment = TextActor.Alignment.Center;
            t3 = new TextActor("Console", "Then try executing \"ChangeSize(3.14)\" or whatever number suits your fancy.");
            t3.Position = new Vector2(0, -5.5f);
            t3.TextAlignment = TextActor.Alignment.Center;
            AddToScreen(t1);
            AddToScreen(t2);
            AddToScreen(t3);

            //Demo housekeeping below this point. 
	        #region Demo housekeeping
	        TextActor fileLoc = new TextActor("ConsoleSmall", "DemoScreenConsole.cs, simple_actor.adf");
	        fileLoc.Position = World.Instance.Camera.ScreenToWorld(5, 755);
	        fileLoc.Color = new Color(.3f, .3f, .3f);
	        AddToScreen(fileLoc);
	        #endregion
        }

        public void AddTexture()
        {
            a.SetSprite("Images\\angel");
        }

        public void ChangeSize(float fsize)
        {
            a.Size = new Vector2(fsize, fsize);
        }
    }
}
