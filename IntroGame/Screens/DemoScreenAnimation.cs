﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AngelMono.Infrastructure;
using AngelMono.Actors;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace IntroGame.Screens
{
    public class DemoScreenAnimation : Screen
    {
        private TextActor t1;
        private Actor a;

        public DemoScreenAnimation()
        {
            
        }

        public override void Enter()
        {
            a = ActorFactory.Instance.CreateActor("devo");
            a.PlayAnimation("Idle");

            AddToScreen(a);

            #region Demo Housekeeping
            String outputText = "You can use tiled sprites and define named animations in data files.";
            outputText += "\n\nPress B, Y, and X to see different named animations.";
            t1 = new TextActor("Console", outputText);
            t1.Position = new Vector2(0.0f, 3.5f);
            t1.TextAlignment = TextActor.Alignment.Center;
            AddToScreen(t1);

            TextActor fileLoc = new TextActor("ConsoleSmall", "DemoScreenAnimation.cs, devo.adf, devo.anims");
            fileLoc.Position = World.Instance.Camera.ScreenToWorld(5, 755);
            fileLoc.Color = new Color(.3f, .3f, .3f);
            AddToScreen(fileLoc);
            #endregion
        }

        public override void Update(GameTime time)
        {
            GamePadState currentState = GamePad.GetState(PlayerIndex.One);
            if (currentState.Buttons.B == ButtonState.Pressed)
            {
                a.PlayAnimation("Run");
            }
            else if (currentState.Buttons.Y == ButtonState.Pressed)
            {
                a.PlayAnimation("Fall");
            }
            else if (currentState.Buttons.X == ButtonState.Pressed)
            {
                a.PlayAnimation("Idle");
            }
        }
    }
}
