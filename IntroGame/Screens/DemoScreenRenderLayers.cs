﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AngelMono.Actors;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using AngelMono.Infrastructure;
using Microsoft.Xna.Framework.Input;

namespace IntroGame.Screens
{
    public class DemoScreenRenderLayers : Screen
    {
        private Actor a1;
        private Actor a2;
        private TextActor t1;
        private TextActor t2;

        public override void Enter()
        {
            a1 = new Actor();
            a1.Size = new Vector2(5.0f, 5.0f);
            a1.Color = Color.Blue;
            a1.Position = new Vector2(-1f, -1f);

            a2 = new Actor();
            a2.Size = new Vector2(5.0f, 5.0f);
            a2.Color = Color.Red;
            a2.Position = new Vector2(1f, 1f);
            a2.Layer = 1;

            AddToScreen(a1);
            AddToScreen(a2);
            
            t1 = new TextActor("Console", "These Actors overlap.");
	        t1.Position = new Vector2(0f, 5.5f);
            t1.TextAlignment = TextActor.Alignment.Center;
	        AddToScreen(t1);
	        t2 = new TextActor("Console", "Use the controller's bumper buttons to change their layer ordering.");
	        t2.Position = new Vector2(0f, 4.5f);
            t2.TextAlignment = TextActor.Alignment.Center;
	        AddToScreen(t2);

            //Demo housekeeping below this point. 
	        #region Demo Housekeeping
	        TextActor fileLoc = new TextActor("ConsoleSmall", "DemoScreenRenderLayers.cs");
	        fileLoc.Position = World.Instance.Camera.ScreenToWorld(5, 755);
	        fileLoc.Color = new Color(.3f, .3f, .3f);
	        AddToScreen(fileLoc);
	        #endregion
        }

        public override void Update(GameTime aTime)
        {
            //NOTE: a2 has been added to layer one, so this function moves a1 around it.
            GamePadState padState = GamePad.GetState(PlayerIndex.One);
            if (padState.Buttons.LeftShoulder == ButtonState.Pressed)
            {
                World.Instance.UpdateLayer(a1, 0); //moves the actor to the requested layer
            }
            else if (padState.Buttons.RightShoulder == ButtonState.Pressed)
            {
                World.Instance.UpdateLayer(a1, 2);
            }
        }
    }
}
