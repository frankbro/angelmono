﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AngelMono.Actors;
using AngelMono.Infrastructure;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace IntroGame.Screens
{
    public class DemoScreenLayeredCollisionLevelFile : Screen
    {
        private TextActor t1;
        private TextActor t2;
        private TextActor t3;

        public override void Enter()
        {
            //Loads the file from Config\Levels\layeredcollisionlevel_demo.lvl
	        ActorFactory.Instance.LoadLevel("layeredcollisionlevel_demo");

	        //All the magic happens in the level file!
            t1 = new TextActor("Console", "These new Actors were assigned layers in the level file.");
	        t1.Position = new Vector2(0.0f, 5.5f);
	        t1.TextAlignment = TextActor.Alignment.Center;
            t1.Layer = 10;
	        AddToScreen(t1);
	        t2 = new TextActor("Console", "Layer names are defined in autoexec.cfg (note variables starting with \"layer_\"),");
	        t2.Position = new Vector2(0.0f, 4.5f);
	        t2.TextAlignment = TextActor.Alignment.Center;
            t2.Layer = 10;
	        AddToScreen(t2);
	        t3 = new TextActor("Console", "and assigned to Actors by ActorFactorySetLayerName <layerName>.");
	        t3.Position = new Vector2(0.0f, 3.5f);
	        t3.TextAlignment = TextActor.Alignment.Center;
            t3.Layer = 10;
	        AddToScreen(t3);

            #region Demo housekeeping
	        TextActor fileLoc = new TextActor("ConsoleSmall", "DemoScreenLayeredCollisionLevelFile.cs, layeredcollisionlevel_demo.lvl,");
	        TextActor fileLoc2 = new TextActor("ConsoleSmall", "      autoexec.cfg");
	        fileLoc.Position = World.Instance.Camera.ScreenToWorld(5, 735);
	        fileLoc.Color = new Color(.3f, .3f, .3f);
            fileLoc.Layer = 10;
            AddToScreen(fileLoc);
	        fileLoc2.Position = World.Instance.Camera.ScreenToWorld(5, 755);
	        fileLoc2.Color = new Color(.3f, .3f, .3f);
            fileLoc2.Layer = 10;
	        AddToScreen(fileLoc2);
	        
            Actor[] spawnedActors = TagCollection.Instance.GetObjectsTagged("spawned");
            foreach (Actor a in spawnedActors)
                _WorldObjects.Add(a);
	        #endregion
        }
    }
}
