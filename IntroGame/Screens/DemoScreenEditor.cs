﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AngelMono.Infrastructure;
using AngelMono.Actors;
using Microsoft.Xna.Framework;

namespace IntroGame.Screens
{
    public class DemoScreenEditor : Screen
    {
        private TextActor t1;
        private Actor a;

        public override void Enter()
        {
            a = ActorFactory.Instance.CreateActor("simple_actor");
            a.Position = new Vector2(0, -3);

            AddToScreen(a);

            #region Demo Housekeeping
            String outputText = "Angel has an in game editor too!  Hit F9 to activate it.";
            outputText += "\n\nIt's simple click and drag.";
            outputText += "\n\nAdd new actors and change their properties in the property panel.";
            outputText += "\n\n\n\nOther Controls: Shift - Rotate, Alt - Scale, Control - Clone";

            t1 = new TextActor("Console", outputText);
            t1.Position = new Vector2(0.0f, 6.0f);
            t1.TextAlignment = TextActor.Alignment.Center;
            AddToScreen(t1);

            TextActor fileLoc = new TextActor("ConsoleSmall", "DemoScreenAnimation.cs, devo.adf, devo.anims");
            fileLoc.Position = World.Instance.Camera.ScreenToWorld(5, 755);
            fileLoc.Color = new Color(.3f, .3f, .3f);
            AddToScreen(fileLoc);
            #endregion
        }
    }
}
