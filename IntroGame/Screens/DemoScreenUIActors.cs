﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AngelMono.Infrastructure;
using AngelMono.Actors;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace IntroGame.Screens
{
    public class DemoScreenUIActors : Screen
    {
        public Actor a;
        public Actor ui1;
        public Actor ui2;
        public TextActor t1;
        public TextActor t2;

        public override void Enter()
        {
            a = new Actor();
            a.Size = new Vector2(4.0f, 4.0f);
            a.Color = new Color(1.0f, 1.0f, 0.0f, 0.5f);
            AddToScreen(a);

            // UI Actors are just regular actors that are added to the UI
            // render group, which is updated and rendered after all Actors in the World
            ui1 = new Actor();
            ui1.Size = new Vector2(2.0f, 2.0f);
            ui1.Color = new Color(0.0f, 1.0f, 0.0f, 1.0f);
            ui1.Position = new Vector2(-6.0f, -6.0f);
            // This calls UI.Instance.Add(ui1) and keeps track of the actor for demo purposes
            AddToScreenUI(ui1);

            // The problem with this approach is that you need to know which actors are in
            // the world, and which are on the UI is you want to add or remove them.
            ui2 = new Actor();
            ui2.Size = new Vector2(2.0f, 2.0f);
            ui2.Color = new Color(0.0f, 1.0f, 0.0f, 1.0f);
            ui2.Position = new Vector2(6.0f, -6.0f);
            AddToScreenUI(ui2);

            t1 = new TextActor("Console", "Actors can also be bound to the UI, which means they won't move");
            t1.Position = new Vector2(0, 4.5f);
            t1.TextAlignment = TextActor.Alignment.Center;
            t2 = new TextActor("Console", "when the world camera moves. Try moving the camera with the right thumbstick.");
            t2.Position = new Vector2(0, 3.5f);
            t2.TextAlignment = TextActor.Alignment.Center;
            AddToScreen(t1);
            AddToScreen(t2);

            //Demo housekeeping below this point. 
            #region Demo Housekeeping
            TextActor fileLoc = new TextActor("ConsoleSmall", "DemoScreenUIActor.cs");
            fileLoc.Position = World.Instance.Camera.ScreenToWorld(5, 755);
            fileLoc.Color = new Color(.3f, .3f, .3f);
            AddToScreen(fileLoc);
            #endregion
        }

        public override void Update(GameTime aTime)
        {
            //Update position based on thumbstick

            //NOTE: by default, the thumbstick has a dead zone around the middle where
            //  it will report position as 0. This prevents jitter when the stick isn't
            //  being touched.

            GamePadState padState = GamePad.GetState(PlayerIndex.One);
            Vector2 position = new Vector2(
                3.0f * padState.ThumbSticks.Left.X,
                3.0f * padState.ThumbSticks.Left.Y
            );

            //Update the position with our calculated values. 
            a.Position = position;

            //Every tick, update the rotation if B is held down
            if (padState.Buttons.B == ButtonState.Pressed)
            {
                a.Rotation = a.Rotation + (90.0f * (float)aTime.ElapsedGameTime.TotalSeconds); //90 degrees per second
                if (a.Rotation > 360.0f)
                {
                    a.Rotation = a.Rotation - 360.0f;
                }
            }


            //Doing the same math we did above for the regular Actor, but this
            //  time applying the position changes to the Camera singleton. 
            Vector3 camPos = new Vector3(
                5.0f * padState.ThumbSticks.Right.X,
                5.0f * padState.ThumbSticks.Right.Y,
                World.Instance.Camera.Position.Z
            );

            World.Instance.Camera.Position = camPos;
        }
    }
}
