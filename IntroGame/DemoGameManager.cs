﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AngelMono.Infrastructure;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using IntroGame.Screens;
using AngelMono.Infrastructure.Console;
using Microsoft.Xna.Framework.Audio;
using AngelMono.Messaging;

namespace IntroGame
{
    class DemoGameManager : DefaultGameManager
    {
        private List<Screen> _screens = new List<Screen>();
        private int _current;
        private SoundEffect _click;

        public DemoGameManager()
        {
            // Allow MoveForards and MoveBackwards to occur both in the console
            // and through input bindings.
            DeveloperConsole.Instance.ItemManager.AddCommand("MoveForwards", new ConsoleCommandHandler(MoveForwards));
            DeveloperConsole.Instance.ItemManager.AddCommand("MoveBackwards", new ConsoleCommandHandler(MoveBackwards));

            Switchboard.Instance["MoveForwards"] += new MessageHandler(x => MoveForwards(null));
            Switchboard.Instance["MoveBackwards"] += new MessageHandler(x => MoveBackwards(null));

            FontCache.Instance.RegisterFont("Fonts\\Inconsolata24", "Console");
            FontCache.Instance.RegisterFont("Fonts\\Inconsolata12", "ConsoleSmall");

            _screens.Add(new DemoScreenStart());                        // 0
            _screens.Add(new DemoScreenInstructions());                 // 1
            _screens.Add(new DemoScreenSimpleActor());                  // 2
            _screens.Add(new DemoScreenRenderLayers());                 // 3
            _screens.Add(new DemoScreenMovingActor());                  // 4
            _screens.Add(new DemoScreenUIActors());                     // 5
            _screens.Add(new DemoScreenDefFile());                      // 6
            _screens.Add(new DemoScreenLevelFile());                    // 7
            _screens.Add(new DemoScreenBindingInstructions());          // 8
            _screens.Add(new DemoScreenParticleActor());                // 9
            _screens.Add(new DemoScreenPhysicsActor());                 // 10
            _screens.Add(new DemoScreenCollisions());                   // 11
            _screens.Add(new DemoScreenCollisionLevelFile());	        // 12
            _screens.Add(new DemoScreenLayeredCollisionLevelFile());	// 13
            _screens.Add(new DemoScreenMessagePassing());               // 14
            _screens.Add(new DemoScreenIntervals());                    // 15
            _screens.Add(new DemoScreenAnimation());                    // 16
            _screens.Add(new DemoScreenConsole());                      // 17
            //_screens.Add(new DemoScreenLogs());                         // 18
            _screens.Add(new DemoScreenPathfinding());                  // 19
            _screens.Add(new DemoScreenEditor());                       // 20
            _screens.Add(new DemoScreenByeBye());                       // 21

            int startingIndex = 0;
            if (_screens.Count > startingIndex)
            {
                ChangeScreen(_screens[startingIndex]);
            }
            else
            {
                _current = -1;
            }

            _click = World.Instance.Game.Content.Load<SoundEffect>("Sounds\\click");
        }

        public object MoveForwards(object[] aParams)
        {
            if (_current >= 0 && (_current < _screens.Count - 1))
            {
                ChangeScreen(_screens[++_current]);
            }

            _click.Play();

            return null;
        }

        public object MoveBackwards(object[] aParams)
        {
            if (_current > 0)
            {
                ChangeScreen(_screens[--_current]);
            }

            _click.Play();

            return null;
        }

        public object MoveTo(object[] aParams)
        {
            DeveloperConsole.VerifyArgs(aParams, typeof(float));
            int ipage = (int)aParams[0];

            if (ipage > 0 && ipage < _screens.Count)
            {
                _current = ipage;
                ChangeScreen(_screens[_current]);
            }

            return null;
        }

        public override void Render(GameTime aTime, Camera aCamera, GraphicsDevice aDevice, SpriteBatch aBatch)
        {
            Color textColor = new Color(0.5f, 0.5f, 0.5f);
            SpriteFont font = FontCache.Instance["ConsoleSmall"];

            string infoString = "";
            int xOffset = 0;
            if (_current == 0)
            {
                infoString = "[A]: Next ";
                xOffset = 925;
            }
            else if (_current == _screens.Count - 1)
            {
                infoString = "[Back]: Previous";
                xOffset = 870;
            }
            else
            {
                infoString = "[A]: Next [Back]: Previous";
                xOffset = 785;
            }

            aBatch.Begin();
            aBatch.DrawString(font, infoString, new Vector2(xOffset, 745), textColor);
            aBatch.End();
        }
    }
}
